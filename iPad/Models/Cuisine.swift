import UIKit
import SwiftyJSON

class Cuisine: NSObject {
    var id = ""
    var name = ""
    var name_ar = ""
    override init() {
    }
    init(fromJson json:JSON!) {
        if json.isEmpty{
            return
        }
        id = json["id"].stringValue
        name = json["name"].stringValue
        name_ar = json["name_ar"].stringValue
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let obj = CuisineDB(context: context)
        obj.id = id
        obj.name = name
        obj.name_ar = name_ar
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
}
