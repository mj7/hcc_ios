//
//  Session.swift
//
//  Created by Pranav Goswami on 11/24/17.
//  Copyright © 2017 Pranav Goswami. All rights reserved.
//
import UIKit
import SwiftyJSON
let APPLE_LANGUAGE_KEY = "AppleLanguages"
class Session : NSObject{
    static let sharedInstance = Session()
    
    let userDefault = UserDefaults.standard
    
    override init(){
        
    }
    func getRecentSearch() -> [String] {
        let  array = userDefault.stringArray(forKey: "recentSearch") ?? [String]()
        return array
    }
    
    func addRecentSearch(text:String){
    
        var  array = userDefault.stringArray(forKey: "recentSearch") ?? [String]()
        
        if !array.contains(text) {
            if array.count >= 5 {
                array.remove(at: 0)
            }
            array.append(text)
            userDefault.set(array, forKey: "recentSearch")
            userDefault.synchronize()
        }
    }
    
    func getLanguage() -> String {
        
        if userDefault.object(forKey: "language") == nil {
            setLanguage(val: "1")
        }
        return userDefault.string(forKey: "language") ?? "1"
    }
    func getLanguageCode() -> String {
        if getLanguage() == "1" {
            return "en"
        }
        return "ar"
    }
    func setLanguage(val:String) {
        userDefault.set(val, forKey: "language")
        userDefault.synchronize()
    }
    func switchLanguage() {
        if getLanguage() == "1" {
            setLanguage(val: "2")
            setAppleLAnguageTo("ar")
        }else{
            setLanguage(val: "1")
            setAppleLAnguageTo("en")
        }
    }
    
    /// set @lang to be the first in Applelanguages list
    func setAppleLAnguageTo(_ lang: String) {
        userDefault.set([lang], forKey: APPLE_LANGUAGE_KEY)
        userDefault.synchronize()
    }
    func MethodSwizzleGivenClassName(cls: AnyClass, originalSelector: Selector, overrideSelector: Selector) {
        let origMethod: Method = class_getInstanceMethod(cls, originalSelector)!;
        let overrideMethod: Method = class_getInstanceMethod(cls, overrideSelector)!;
        if (class_addMethod(cls, originalSelector, method_getImplementation(overrideMethod), method_getTypeEncoding(overrideMethod))) {
            class_replaceMethod(cls, overrideSelector, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
        } else {
            method_exchangeImplementations(origMethod, overrideMethod);
        }
    }
    func DoTheSwizzling() {
        MethodSwizzleGivenClassName(cls: Bundle.self, originalSelector: #selector(Bundle.localizedString(forKey:value:table:)), overrideSelector: #selector(Bundle.specialLocalizedStringForKey(key:value:table:)))
         MethodSwizzleGivenClassName(cls: UITextField.self, originalSelector: #selector(UITextField.layoutSubviews), overrideSelector: #selector(UITextField.customlayoutSubviews))
        MethodSwizzleGivenClassName(cls: UILabel.self, originalSelector: #selector(UILabel.layoutSubviews), overrideSelector: #selector(UILabel.customlayoutSubviews))
         MethodSwizzleGivenClassName(cls: UIButton.self, originalSelector: #selector(UIButton.layoutSubviews), overrideSelector: #selector(UIButton.customlayoutSubviews))
    }
    
}
//MARK: - UIButton Extension
extension UIButton {
    @objc public func customlayoutSubviews() {
        self.customlayoutSubviews()
        if Session.sharedInstance.getLanguage() != "1" && self.contentHorizontalAlignment == .left {
            self.contentHorizontalAlignment = .right
        }
        if self.title(for: .normal) != nil {
            self.setTitle(NSLocalizedString(self.title(for: .normal)!, comment: ""), for: .normal)
        }
    }
}
extension UILabel {
    @objc public func customlayoutSubviews() {
        self.customlayoutSubviews()
        if self.text != nil {
            self.text = NSLocalizedString(self.text!, comment: "")
        }
        if Session.sharedInstance.getLanguage() != "1" && self.textAlignment == .left {
            self.textAlignment = .right
        }
    }
}
extension UITextField {
    @objc public func customlayoutSubviews() {
        self.customlayoutSubviews()
        if self.placeholder != nil {
            self.placeholder = NSLocalizedString(self.placeholder!, comment: "")
        }
        if Session.sharedInstance.getLanguage() == "2" {
            self.textAlignment = .right
        }else{
            self.textAlignment = .left
        }
    }
}
extension Bundle {
    @objc func specialLocalizedStringForKey(key: String, value: String?, table tableName: String?) -> String {
        if self == Bundle.main {
            let currentLanguage = Session.sharedInstance.getLanguageCode()
            var bundle = Bundle();
            if let _path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
                bundle = Bundle(path: _path)!
            } else {
                let _path = Bundle.main.path(forResource: "Base", ofType: "lproj")!
                bundle = Bundle(path: _path)!
            }
            return (bundle.specialLocalizedStringForKey(key: key, value: value, table: tableName))
        } else {
            return (self.specialLocalizedStringForKey(key: key, value: value, table: tableName))
        }
    }
}
