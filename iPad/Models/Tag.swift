import UIKit
import SwiftyJSON

class Tag: NSObject {
    var filter_name_ar = ""
    var filter_id = ""
    var filter_name = ""
    override init() {
    }
    init(fromJson json:JSON!) {
        if json.isEmpty{
            return
        }
        filter_name_ar = json["name_ar"].stringValue
        filter_id = json["id"].stringValue
        filter_name = json["name"].stringValue
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let obj = TagDB(context: context)
        obj.id = filter_id
        obj.name = filter_name
        obj.name_ar = filter_name_ar
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
}
