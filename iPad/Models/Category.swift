import UIKit
import SwiftyJSON

class Category: NSObject {
    var id = ""
    var name = ""
    var name_ar = ""
    var logo = ""
    var parent_id = ""
    override init() {
    }
    init(fromJson json:JSON!) {
        if json.isEmpty{
            return
        }
        id = json["id"].stringValue
        name = json["name"].stringValue
        name_ar = json["name_ar"].stringValue
        logo = json["logo"].stringValue
        parent_id = json["parent_id"].stringValue
       
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let obj = CategoryDB(context: context)
        obj.id = id
        obj.name = name
        obj.name_ar = name_ar
        obj.logo = logo
        obj.parentId = parent_id
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
}
