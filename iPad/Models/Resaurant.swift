import UIKit
import SwiftyJSON
import CoreData
class Restaurant: NSObject {
    var id = ""
    var name = ""
    var name_ar = ""
    var phone = ""
    var cuisine = ""
    var cuisine_ar = ""
    var background_image = ""
    var logo = ""
    var descriptionText = ""
    var descriptionText_ar = ""
    var details_background_image = ""
    var arrayCuisine = [Cuisine]()
    override init() {
    }
    init(fromJson json:JSON!) {
        if json.isEmpty{
            return
        }
    
        id = json["id"].stringValue
        background_image = json["background_image"].stringValue
        name = json["name"].stringValue
        name_ar = json["name_ar"].stringValue
        descriptionText = json["description"].stringValue
        descriptionText_ar = json["description_ar"].stringValue
        details_background_image = json["details_background_image"].stringValue
        phone = json["phone"].stringValue
        var array = [String]()
        var array_ar = [String]()
        for obj in json["cuisine"].arrayValue {
            array.append(obj["name"].stringValue)
            array_ar.append(obj["name_ar"].stringValue)
        }
        cuisine = array.joined(separator: ", ")
        cuisine_ar = array_ar.joined(separator: ", ")
        
        logo = json["logo"].stringValue
    }
    func createDB(objectDB:TypeDB){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let obj = RestaurantDB(context: context)
        obj.id = id
        obj.name = name
        obj.name_ar = name_ar
        obj.background = background_image
        obj.detail = descriptionText
        obj.detail_ar = descriptionText_ar
        obj.phone = phone
        obj.cuisine = cuisine
        obj.cuisine_ar = cuisine_ar
        obj.logo = logo
        obj.details_background_image = details_background_image
        obj.type = objectDB
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
}
