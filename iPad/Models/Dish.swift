import UIKit
import SwiftyJSON

class Dish: NSObject {
    var id = ""
    var name = ""
    var name_ar = ""
    var descriptionText = ""
    var descriptionText_ar = ""
    var main_image = ""
    var price = ""
    var price_ar = ""
    var price_number = ""
    var categoryId = ""
    var gallery = [String]()
    var tags = [Tag]()
    override init() {
    }
    init(fromJson json:JSON!,category_id:String) {
        if json.isEmpty{
            return
        }
        categoryId = category_id
        id = json["id"].stringValue
        name = json["name"].stringValue
        name_ar = json["name_ar"].stringValue
        descriptionText = json["description"].stringValue
        descriptionText_ar = json["description_ar"].stringValue
        main_image = json["main_image"].stringValue
        price = json["price"].stringValue
        price_ar = json["price_ar"].stringValue
        price_number = json["price_number"].stringValue
        
        for obj in json["filters"].arrayValue {
            let tag = Tag()
            tag.filter_name_ar = obj["filter_name_ar"].stringValue
            tag.filter_id = obj["filter_id"].stringValue
            tag.filter_name = obj["filter_name"].stringValue
            tags.append(tag)
        }
        
        for url in json["gallery"].arrayValue {
            gallery.append(url.stringValue)
        }
        
        
        
      
        
        
    }
    func createDB(tagset:NSMutableSet){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let obj = DishDB(context: context)
        obj.id = id
        obj.name = name
        obj.name_ar = name_ar
        obj.categoryID = categoryId
        obj.detail = descriptionText
        obj.detail_ar = descriptionText_ar
        obj.main_image = main_image
        obj.price = price
        obj.price_ar = price_ar
        obj.price_number = price_number
        obj.tags = tagset
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        for url in gallery {
            let objURL = GalleryDB(context: context)
            objURL.url = url
            objURL.dish = obj
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
    }
}
