//
//  TacoDialogView.swift
//  Demo
//
//  Created by Tim Moose on 8/12/16.
//  Copyright © 2016 SwiftKick Mobile. All rights reserved.
//

import UIKit
import SwiftMessages

class AddToCartAlert: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonViewCart: UIButton!
    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var labelTotal: UILabel!
    
}
