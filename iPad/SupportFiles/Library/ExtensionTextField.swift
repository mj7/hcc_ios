//
//  ExtensionTextField.swift
//  SLRB
//
//  Created by Fabri IT on 5/31/17.
//  Copyright © 2017 Fabri IT. All rights reserved.
//

import UIKit

extension UITextField
{
    func padding()
    {
        let paddingLeft = UIView(frame: CGRect(x: 0, y: 5, width: 20, height: 5))
        self.leftView = paddingLeft
        self.leftViewMode = UITextField.ViewMode.always
        
        let paddingRight = UIView(frame: CGRect(x: 0, y: 5, width: 20, height: 5))
        self.rightView = paddingRight
        self.rightViewMode = UITextField.ViewMode.always
    }
    
    func rightImage(image: String){
        
        let paddingRight = UIView(frame: CGRect(x: 0, y: 0, width: 30 + 5, height: 30))
        self.rightView = paddingRight
        self.rightViewMode = UITextField.ViewMode.always
        
        let icn : UIImage = UIImage(named: image as String)!
        let imageView = UIImageView(image: icn)
        imageView.frame = CGRect(x: 5, y: 0, width: 30, height: 30)
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        self.leftViewMode = UITextField.ViewMode.always
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 30 + 5, height: 30))
        view.addSubview(imageView)
        self.rightView = view
    }
    
    func leftImage(image: String)
    {
        let paddingRight = UIView(frame: CGRect(x: 0, y: 0, width: 30 + 5, height: 30))
        self.rightView = paddingRight
        self.rightViewMode = UITextField.ViewMode.always
        
        let icn : UIImage = UIImage(named: image as String)!
        let imageView = UIImageView(image: icn)
        imageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        self.leftViewMode = UITextField.ViewMode.always
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 30 + 5, height: 30))
        view.addSubview(imageView)
        self.leftView = view
    }
    func setExtraImage(image:UIImage){
        let size = 10
        let paddingRight = UIView(frame: CGRect(x: 0, y: 0, width: size, height: size))
        self.rightView = paddingRight
        self.rightViewMode = UITextField.ViewMode.always
        let view = UIView(frame: CGRect(x: 0, y: 0, width: size, height: size))
        let imageView = UIImageView(image: image)
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        self.leftViewMode = UITextField.ViewMode.always
        imageView.frame = CGRect(x: 0, y: 0, width: size, height: size)
        view.addSubview(imageView)
//        if Session.sharedInstance.getLanguage() == "0"{
//            self.rightView = view
//        }else{
//            self.leftView = view
//        }
    }

    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
}
class CustomTextField: UITextField {
    
    
    override func draw(_ rect: CGRect) {
        self.font = UIFont(name: "MyriadPro-Regular", size: 20)
    }
    
    
    @IBInspectable var leftPadding: CGFloat = 0.0 {
        didSet {
            setLeftPadding(leftPadding: leftPadding)
        }
    }
    
    func setLeftPadding(leftPadding: CGFloat) {
        let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: leftPadding+3, height: self.frame.size.height))
        self.leftView = leftPaddingView
        self.leftViewMode = UITextField.ViewMode .always
    }
    
    @IBInspectable var rightPadding: CGFloat = 0.0 {
        didSet {
            setRightPadding(rightPadding: rightPadding)
        }
    }
    
    func setRightPadding(rightPadding: CGFloat) {
        let rightPaddingView = UIView(frame: CGRect(x: 0, y: self.frame.size.width - rightPadding, width: rightPadding, height: self.frame.size.height))
        self.rightView = rightPaddingView
        self.rightViewMode = UITextField.ViewMode .always
    }
    
    @IBInspectable var padding: CGFloat = 0.0 {
        didSet {
            setPadding()
        }
    }
    
    func setPadding() {
        setLeftPadding(leftPadding: padding)
        setRightPadding(rightPadding: padding)
    }
    
    /// A UIColor value that determines text color of the placeholder label
    @IBInspectable open var placeholderColor:UIColor = UIColor.lightGray {
        didSet {
            self.updatePlaceholder()
        }
    }
    
    /// A UIColor value that determines text color of the placeholder label
    @IBInspectable open var placeholderFont: UIFont? {
        didSet {
            self.updatePlaceholder()
        }
    }
    
    fileprivate func updatePlaceholder() {
        if let
            placeholder = self.placeholder,
            let font = self.placeholderFont ?? self.font {
            self.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [NSAttributedString.Key.foregroundColor:placeholderColor,NSAttributedString.Key.font: font])
        }
    }
}
