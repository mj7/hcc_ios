//
//  ExtensionImageView.swift
//  Alia Flower
//
//  Created by Pranav Goswami on 11/15/17.
//  Copyright © 2017 Pranav Goswami. All rights reserved.
//

import Foundation
import UIKit
import EventKit

extension UIViewController
{
    func addEvent(title:String,notes:String,startDate:Date,endDate:Date,shouldRemind:Bool = true,reminderDate:Date=Date(), completion: @escaping EventResponse) {
        let eventStore : EKEventStore = EKEventStore()
        
        // 'EKEntityTypeReminder' or 'EKEntityTypeEvent'
        
        eventStore.requestAccess(to: .event) { (granted, error) in
            
            if (granted) && (error == nil) {
                let event:EKEvent = EKEvent(eventStore: eventStore)
                
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = notes
                event.calendar = eventStore.defaultCalendarForNewEvents
//                let alarm = EKAlarm.init(absoluteDate: Date.init(timeInterval: -3600, since: event.startDate))
                if shouldRemind {
//                    let alarm = EKAlarm()
//                    let date = self.getDateFromString(date: "03-03-2018 08:30 pm", format: "dd-MM-yyyy hh:mm a")
//                    alarm.relativeOffset = TimeInterval(-2*60*60)
                    let alarm = EKAlarm.init(absoluteDate: reminderDate)
                    event.alarms = [alarm]
                }
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let error as NSError {
                    print("failed to save event with error : \(error)")
                }
                completion(true, nil)
            }else{
                completion(false, error as NSError?)
                print("failed to save event with error : \(error) or access not granted")
            }
        }
    }

}
