//
//  Animator.swift
//  Alia Flower
//
//  Created by Pranav Goswami on 11/16/17.
//  Copyright © 2017 Pranav Goswami. All rights reserved.
//


import UIKit
class Animator {
    static func scaleUp(view:UIView) -> UIViewPropertyAnimator {
        view.transform = CGAffineTransform(scaleX: 0.67, y: 0.67)
        view.alpha = 0
        let scale = UIViewPropertyAnimator(duration:0.5,curve:.easeInOut)
        scale.addAnimations {
            view.alpha = 1
        }
        
        scale.addAnimations({
            view.transform = CGAffineTransform.identity
        }, delayFactor: 0.25)
        
        return scale
    }
}
