//
//  ExtensionImageView.swift
//  Alia Flower
//
//  Created by Pranav Goswami on 11/15/17.
//  Copyright © 2017 Pranav Goswami. All rights reserved.
//

import Foundation
import Kingfisher
import MZDownloadManager
extension UIImageView {
//    func SetImage(imageName:String) {
//        self.image = nil
//        self.kf.indicatorType = .activity
//        if  let url = URL(string:imageName) {
//            ImageCache.default.maxCachePeriodInSecond = -1
//            let resource = ImageResource(downloadURL: url, cacheKey: imageName)
//
//
//            self.kf.setImage(with: resource, placeholder: nil, options: [.transition(ImageTransition.fade(1))], progressBlock: nil) { (image, error, cashType, imageUrl) in
//                if error == nil {
//                    //print(imageName)
//                    ImageCache.default.store(image!, forKey: imageName)
//                }
//            }
//        }
//    }
    func setOfflineImage(imageName:String) {
        self.setImage(imageName: imageName)
//        let myDownloadPath = MZUtility.baseFilePath + "/My_Downloads"
//        let url = myDownloadPath + "/" + (imageName as NSString).lastPathComponent
//        if FileManager.default.fileExists(atPath: url) {
////            self.image = UIImage(contentsOfFile: url)
//            self.setImage(imageName: "file://\(url)")
//        }else{
//            self.setImage(imageName: imageName)
//        }
    }
    func setImage(imageName:String,isLoading:Bool = true) {
            print(imageName)
        if isLoading {
            self.image = nil
            self.kf.indicatorType = .activity
        }
        if  let url = URL(string:imageName) {
            self.kf.setImage(with: url, placeholder: self.image, options: [.transition(ImageTransition.fade(1))])
        }
    }
    func setHeaderImageDesign(imageName:String) {
        self.setImage(imageName: imageName)
        self.layer.cornerRadius = 17
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
    }
}
@IBDesignable
class RoundedUIImageView: UIImageView {
    @IBInspectable var round: Bool = true {
        didSet {
            self.clipsToBounds = true
            self.layer.cornerRadius = self.frame.width / 2
            //self.layer.borderWidth = 2.5
        }
    }
}
