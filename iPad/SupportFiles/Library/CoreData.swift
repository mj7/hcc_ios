//
//  CoreData.swift
//  Manzil
//
//  Created by Pranav Goswami on 8/17/17.
//  Copyright © 2017 Pranav Goswami. All rights reserved.
//
import UIKit
import Foundation
import CoreData
extension UIViewController {
    func getContext() -> NSManagedObjectContext{
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }
    func saveContext(){
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    func fetchAllData(_ tableName: String) -> [Any] {
        
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult>  = NSFetchRequest(entityName: tableName)
        var array = [Any]()
        do {
            array = try managedObjectContext.fetch(fetchRequest)
        }catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        return array
    }
   
    
    func fetchData(_ tableName: String, predicate:String,id:String) -> [Any]{
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let request:NSFetchRequest<NSFetchRequestResult>  = NSFetchRequest(entityName: tableName)
        request.predicate = NSPredicate(format: predicate,id)
        
        var array = [Any]()
        do {
            array = try managedObjectContext.fetch(request)
        } catch {
            fatalError("Failed to fetch: \(error)")
        }
        return array
    }
    func fetchData(_ tableName: String, predicate:NSPredicate) -> [Any]{
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let request:NSFetchRequest<NSFetchRequestResult>  = NSFetchRequest(entityName: tableName)
        request.predicate = predicate
        
        var array = [Any]()
        do {
            array = try managedObjectContext.fetch(request)
        } catch {
            fatalError("Failed to fetch: \(error)")
        }
        return array
    }
//    func fetchData(_ tableName: String, predicate:String,obj:CategoryDB) -> [Any]{
//        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
//        let request:NSFetchRequest<NSFetchRequestResult>  = NSFetchRequest(entityName: tableName)
//        request.predicate = NSPredicate(format: predicate,obj)
//        var array = [Any]()
//        do {
//            array = try managedObjectContext.fetch(request)
//        } catch {
//            fatalError("Failed to fetch: \(error)")
//        }
//        return array
//    }
    func fetchData(_ tableName: String, predicate:String,obj:NSManagedObject) -> [Any]{
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let request:NSFetchRequest<NSFetchRequestResult>  = NSFetchRequest(entityName: tableName)
        request.predicate = NSPredicate(format: predicate,obj)
        var array = [Any]()
        do {
            array = try managedObjectContext.fetch(request)
        } catch {
            fatalError("Failed to fetch: \(error)")
        }
        return array
    }
    func fetchData(_ tableName: String,id:String) -> [Any]{
        let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let request:NSFetchRequest<NSFetchRequestResult>  = NSFetchRequest(entityName: tableName)
        request.predicate = NSPredicate(format: "id == %@",id)
        var array = [Any]()
        do {
            array = try managedObjectContext.fetch(request)
        } catch {
            fatalError("Failed to fetch: \(error)")
        }
        return array
    }
   
    func deleteAllData(_ tableName: String) {
        var array = self.fetchAllData(tableName)
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        for i in 0..<array.count {
            context.delete(array[i] as! NSManagedObject)
        }
        
        do{
            try context.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
//    func manageFavourite(obj:Event){
//        let array = self.fetchData("EventFav", predicate: "recid == %@", id: obj.recid) as! [EventFav]
//        if array.count > 0 {
//            getContext().delete(array[0])
//        }else{
//            
//            let object = EventFav(context: getContext())
//            object.recid = obj.recid
//            object.weblink = obj.weblink
//            object.vWeburl = obj.vWeburl
//            object.vidFilename = obj.detail_image
//            object.viAddress = obj.viAddress
//            object.vCphone = obj.vCphone
//            object.vCemail = obj.vCemail
//            object.totime = obj.totime
//            object.todate = obj.todate
//            object.ticketavailableat = obj.ticketavailableat
//            object.price = obj.price
//            object.infName = obj.infName
//            object.infLocationname = obj.infLocationname
//            object.infLdesc = obj.infLdesc
//            object.infLanguageid = obj.infLanguageid
//            object.fromdate = obj.fromdate
//            object.fromtime = obj.fromtime
//            object.flagPrice = obj.flagPrice
//            object.categoryName = obj.categoryName
//            object.catColor = obj.catColor
//            object.categoryIcon = obj.categoryIcon
//            object.viName = obj.viName
//        }
//        (UIApplication.shared.delegate as! AppDelegate).saveContext()
//    }
}
