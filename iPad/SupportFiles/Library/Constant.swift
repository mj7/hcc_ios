//
//  Constant.swift
//

import Foundation
import UIKit

var appColor:UIColor = #colorLiteral(red: 0.662745098, green: 0.168627451, blue: 0.2431372549, alpha: 1)
var arraySocial = ["https://www.facebook.com/TourismBH",
                   "https://twitter.com/tourismbh/",
                   "https://www.instagram.com/tourismbh/",
                   "https://www.snapchat.com/add/tourismbh"]

var key = "cb4d01bf9ce64debe8c2f61d0ace4b18"
struct BasePath{
//    static var BasePath         = "http://q.wassly.com/api/"
    static var BasePath         = "http://app.dinemenuapp.com/api/" //"http://menu2.iceboxhost.com/api/"
}

struct Path
{
    static let getCuisine                   = "\(BasePath.BasePath)cuisineList"
    static let getRestaurant                = "\(BasePath.BasePath)browseRestaurant"
    static let getCategories                = "\(BasePath.BasePath)listCategories"
    static let getDishes                    = "\(BasePath.BasePath)allDishes"
    static let getTags                      = "\(BasePath.BasePath)filterList"
    static let getTypes                     = "\(BasePath.BasePath)typeList"
    static let bookARestaurant              = "\(BasePath.BasePath)addTableBooking"
}

struct Messages {
    static let somthingWrong = "Something went wrong. Please try again later."
    static let internetNotAvailable = "Internet connection not found. Please try again later."
    static let enterEmail = "Please enter your email"
    static let enterPassword = "Please enter your password"
    static let enterConfirmPassword = "Please enter confirm password"
    static let enterValidPassword = "Password should be at least 6 characters long"
    static let enterValidEmail = "Please enter valid email"
    static let enterFirstName = "Please enter first name"
    static let enterLastName = "Please enter last name"
    static let enterUserName = "Please enter user name"
    static let enterValidConfirmPassword = "Password and confirm Password must be same"
    static let enteroldpassword = "Please enter your old Password"
    static let enterNewpassword = "Please enter your new Password"
    static let ValidPassword = "Old password and New password must be different."
    static let sendSuccess = "Information submitted successfully";
}
