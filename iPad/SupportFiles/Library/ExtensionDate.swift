//
//  ExtensionDate.swift
//  Manzil
//
//  Created by Pranav Goswami on 8/26/17.
//  Copyright © 2017 Pranav Goswami. All rights reserved.
//
import Foundation
import UIKit
extension UIViewController
{

//    func convertFormatOfDate(date: String, originalFormat: String, destinationFormat: String) -> String! {
//
//        // Orginal format :
//        let dateOriginalFormat = DateFormatter()
//        dateOriginalFormat.dateFormat = originalFormat
//
//        // Destination format :
//        let dateDestinationFormat = DateFormatter()
//        dateDestinationFormat.dateFormat = destinationFormat
//
//        // Convert current String Date to NSDate
//        let dateFromString = dateOriginalFormat.date(from: date)
//
//        // Convert new NSDate created above to String with the good format
//        let dateFormated = dateDestinationFormat.string(from: dateFromString!)
//
//        return dateFormated
//
//    }
    func getDateFromString(date: String, format: String) -> Date {
        
        // Orginal format :
        let dateOriginalFormat = DateFormatter()
        dateOriginalFormat.dateFormat = format
        
       
        
        // Convert current String Date to NSDate
        let dateFromString = dateOriginalFormat.date(from: date)
        
        return dateFromString!
        
    }
    func getStringFromDate(date:Date,inFormate: String) -> String {
        let dateDestinationFormat = DateFormatter()
        dateDestinationFormat.dateFormat = inFormate
        let dateFormated = dateDestinationFormat.string(from: date)
        return dateFormated
        
    }
    func getCurrentDate(inFormate: String) -> String {
        let dateDestinationFormat = DateFormatter()
        dateDestinationFormat.dateFormat = inFormate
        let dateFormated = dateDestinationFormat.string(from: Date())
        return dateFormated
        
    }
    func getCurrentDateComponent(with:Int)->Int{
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        switch with {
        case 0:
            return components.year!
        case 1:
            return components.month!
        case 2:
            return components.day!
        default:
            return 0
        }
    }
    func getDateComponentFromString(date:String,format:String, with:Int)->Int{
        let date = getDateFromString(date: date, format: format)
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        
        switch with {
        case 0:
            return components.year!
        case 1:
            return components.month!
        case 2:
            return components.day!
        default:
            return 0
        }
    }
    
    func getDateByAddingComponentFromString(date:String,format:String, with:Int,value:Int)->Date{
        let date = getDateFromString(date: date, format: format)
        let calendar = Calendar.current
        switch with {
        case 2:
            return  calendar.date(byAdding: .year,value:value, to: date)!
        case 1:
            return  calendar.date(byAdding: .month,value:value, to: date)!
        case 0:
            return  calendar.date(byAdding: .day,value:value, to: date)!
        default:
            return Date()
        }
    }
}
