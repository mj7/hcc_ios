//
//  Extension.swift
//

import Foundation
import UIKit
import Alamofire
//import GONMarkupParser
import SafariServices
import MessageUI
import CoreLocation
import SwiftyJSON
//import NVActivityIndicatorView
import SwiftMessages
import SVProgressHUD
//import LMSideBarController

typealias ServiceResponse = (JSON, Error?) -> Void
typealias ServiceResponseNSData = (JSON, NSError?) -> Void
typealias EventResponse = (Bool, NSError?) -> Void
extension UIViewController
{
    
    var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController!.popViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func goToCart(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func isEnglish() -> Bool{
        return Session.sharedInstance.getLanguage() == "1" ? true:false
    }
    func canAddDish(id:String) -> Bool {
        
        
        let cart = self.fetchAllData("CartDB") as! [CartDB]
        let cartId = UserDefaults.standard.string(forKey: "restaurantId")
        if cart.count == 0 ||  cartId == id  || cartId == nil || cartId == ""{
            UserDefaults.standard.set(id, forKey: "restaurantId")
            UserDefaults.standard.synchronize()
            return true
        }
        
        
        
        return false
        
    }
    func ShowTost(message:String){
//        Toast(text: message).show()
//        ToastView.appearance().backgroundColor = UIColor.black
//        ToastView.appearance().textColor = UIColor.white
//        ToastView.appearance().font = UIFont(name: "Helvetica", size: 16)
    }
    
    func showLoading() {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultAnimationType(.native)
        SVProgressHUD.setDefaultMaskType(.black)
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
//        SVProgressHUD.show()
//        let size = CGSize(width: 50, height:50)
//        startAnimating(size, message: nil, type: NVActivityIndicatorType(rawValue:12)!,color:UIColor.white)
        
        self.view.isUserInteractionEnabled = false
    }
    
    func hideLoading() {
//        stopAnimating()
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
        self.view.isUserInteractionEnabled = true
    }
    
    func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
}

//extension UIScrollView{
//    open override func draw(_ rect: CGRect) {
//        super.draw(rect)
//        self.scroll
//    }
//}

extension String
{
    func isValidEmail() -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: self)
    }
    func isEmpty() -> Bool
    {
        let strText = self.trimmingCharacters(in: NSCharacterSet.whitespaces)
        return strText.isEmpty
    }
    func Trimming() -> String
    {
        let strText =  self.trimmingCharacters(in: NSCharacterSet.whitespaces)
        return strText
    }
    
    func firstCharacter() -> String {
        return String(self[self.startIndex]).uppercased()
    }
    
}

extension UIColor {
    
    convenience init(hex: Int) {
        
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
    }
}

extension UIViewController: MFMailComposeViewControllerDelegate {
    func sendEmail(mails:[String],subject:String,body:String) {
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            return
        }
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients(mails)
        composeVC.setSubject(subject)
        composeVC.setMessageBody(body, isHTML: false)
        self.present(composeVC, animated: true, completion: nil)
    }
    
    public func mailComposeController(_ controller: MFMailComposeViewController,
                                      didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension UIViewController
{
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func loadConstraint(){
        if Session.sharedInstance.getLanguage() == "1" {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
    }
    func flipImageViews(subviews: [UIView]) {
        if Session.sharedInstance.getLanguage() == "2" {
            if subviews.count > 0 {
                for subView in subviews {
                    if subView.isKind(of: UIImageView.self) && subView.tag < 0 {
                        let imageView = subView as! UIImageView
                        if let image = imageView.image {
                            imageView.image = UIImage(cgImage: image.cgImage!, scale:image.scale , orientation: UIImage.Orientation.upMirrored)
                        }
                    }
                    flipImageViews(subviews: subView.subviews)
                }
            }
        }
    }
    func flipButtons(subviews: [UIView]) {
        if Session.sharedInstance.getLanguage() == "2" {
            if subviews.count > 0 {
                for subView in subviews {
                    if subView.isKind(of: UIButton.self) && subView.tag < 0 {
                        let imageView = subView as! UIButton
                        if let image = imageView.imageView?.image {
                            //                            let image = imageView.imageView?.image
                            imageView.setImage(UIImage(cgImage: image.cgImage!, scale:image.scale , orientation: UIImage.Orientation.upMirrored), for: .normal)
                        }
                    }
                    flipImageViews(subviews: subView.subviews)
                }
            }
        }
    }
    func loadiFrame(withUrl url:String){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "InAppWebVC") as! InAppWebVC
        vc.urlString = url
        vc.delay = 0.0
        vc.view.frame = CGRect(x: 0, y: 0,width: self.view.frame.size.width,height:self.view.frame.height)
        self.view.addSubview(vc.view)
        addChild(vc)
    }
    func getDistanceBetweenTwoLocation(lat:String,lon:String)->Int{
        
        let userDefaults = UserDefaults.standard
        let latitude = userDefaults.string(forKey: "latitude")
        let longitude = userDefaults.string(forKey: "longitude")
        
        let coordinate₀ = CLLocation(latitude:Double(latitude!)!, longitude: Double(longitude!)!)
        let coordinate₁ = CLLocation(latitude: Double(lat)!, longitude: Double(lon)!)
        
        let distance = coordinate₀.distance(from: coordinate₁) / 1000
        return Int(distance)
    }
    func getHightForView(text:String,font: UIFont, width:CGFloat) -> CGFloat
    {
        let label:UILabel = UILabel(frame: CGRect(x:0,y: 0,width: width,height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.textAlignment = .center
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    func getWidthForView(_ text:String, font:UIFont, height:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: height))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.width
    }
    func shareDailog(text:String){
        var sharingItems = [AnyObject]()
        sharingItems.append(text as AnyObject)
        let activityViewController = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func call(phone:String) {
        let number = phone.replacingOccurrences(of: " ", with: "")
        if let phoneCallURL:URL = URL(string: "telprompt://\(number)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL)
            }
        }
    }
    func loadUrl(webView:UIWebView,url:String){
        webView.loadRequest(URLRequest(url: URL(string:url)!))
    }
    
    func openUrl(url:String){
        //        if UIApplication.shared.canOpenURL(URL(string:url)!){
        //            UIApplication.shared.openURL(URL(string: url)!)
        //        }
        if let urlLink = URL(string: url){
            let safariVC = SFSafariViewController(url:urlLink)
            self.present(safariVC, animated: true, completion: nil)
        }
    }
    
    func openGoogleMapWithDirection(latitude:String,longitude:String){
        let urlString = "saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving"
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(URL(string:"comgooglemaps://?" + urlString)!)
        } else {
            openUrl(url: "https://map.google.com?" + urlString)
        }
    }
    
    func openGoogleMap(latitude:String,longitude:String){
        let urlString = "saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving"
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(URL(string:"comgooglemaps://?" + urlString)!)
        } else {
            openUrl(url: "https://map.google.com?" + urlString)
        }
    }
    func openGoogleMap(query:String){
        let urlString = "q=" + query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(URL(string:"comgooglemaps://?" + urlString)!)
        } else {
            openUrl(url: "https://map.google.com?" + urlString)
        }
    }
   
//    func getAttributedStringFromHTML(data:String,lineSpecing:Bool = true) -> NSAttributedString {
//        var htmlText = data.replacingOccurrences(of: "\r\n", with: "\n")
//        htmlText = htmlText.replacingOccurrences(of: "\n\n", with: "\n")
//        htmlText = htmlText.replacingOccurrences(of: "\n<p>", with: "<p>")
//        htmlText = htmlText.replacingOccurrences(of: "<p>&nbsp;</p>", with: "")
//        //print(htmlText)
//        let parser = GONMarkupParser.default()
//https://i.diawi.com/m6heBu
//        let rawResult = parser?.attributedString(from: htmlText)
//
//        let attributedString = NSMutableAttributedString(attributedString:parser!.attributedString(from: rawResult!.string))
//
//        if lineSpecing{
//            let paragraphStyle = NSMutableParagraphStyle()
////            paragraphStyle.lineSpacing = 6
//            paragraphStyle.alignment = .justified
////            if Session.sharedInstance.getLanguage() == "1"{
////                paragraphStyle.alignment = .right
////            }
//            //        paragraphStyle.paragraphSpacing = 1
//            //        paragraphStyle.paragraphSpacingBefore = 0
//            attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
//        }
//        return attributedString
//    }
    func convertFormatOfDate(date: String, originalFormat: String, destinationFormat: String) -> String! {
        
        // Orginal format :
        let dateOriginalFormat = DateFormatter()
        dateOriginalFormat.dateFormat = originalFormat
        
        // Destination format :
        let dateDestinationFormat = DateFormatter()
        dateDestinationFormat.dateFormat = destinationFormat
        
        // Convert current String Date to NSDate
        let dateFromString = dateOriginalFormat.date(from: date)
        
        // Convert new NSDate created above to String with the good format
        if dateFromString != nil {
        let dateFormated = dateDestinationFormat.string(from: dateFromString!)
        return dateFormated
        }
        return date
        
    }
    
    func addNavigationView(_ title:String,isBackHidden:Bool = false,isPlusHidden:Bool = true) -> NavigationVC{
        let bgView = UIView()
        bgView.frame = CGRect(x: 0, y: 0,width: self.view.frame.size.width,height:44)
        bgView.backgroundColor = #colorLiteral(red: 0.1388770342, green: 0.1380874217, blue: 0.2903159857, alpha: 1)
        self.view.addSubview(bgView)
        
        let navigation = NavigationVC(nibName: "NavigationVC", bundle: nil)
       
        if #available(iOS 11.0, *) {
            self.view.addSubview(navigation.view)
            addChild(navigation)
            navigation.view.translatesAutoresizingMaskIntoConstraints = false
            let guide = self.view.safeAreaLayoutGuide
            navigation.view.trailingAnchor.constraint(equalTo: guide.trailingAnchor).isActive = true
            navigation.view.leadingAnchor.constraint(equalTo: guide.leadingAnchor).isActive = true
            navigation.view.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
            navigation.view.heightAnchor.constraint(equalToConstant: 44).isActive = true
            
        }else{
           navigation.view.frame = CGRect(x: 0, y: 0,width: self.view.frame.size.width,height:64)
            self.view.addSubview(navigation.view)
            addChild(navigation)
        }
        navigation.labelTitle.text = title
        navigation.viewBack.isHidden = isBackHidden
        navigation.buttonPlus.isHidden = isPlusHidden
        UIApplication.shared.statusBarStyle = .lightContent
        return navigation
    }
    
    func showDropDown(_ title:String, arrayData:[DropDown],delegate:DropDownDelegate,status:Int = 1,isSortDropDown:Bool = false){
        var nib = "DropDownVC"
        if isSortDropDown {
            nib = "DropDownSmallVC"
        }
        let vc = DropDownVC(nibName: nib, bundle: nil)
        vc.view.frame = CGRect(x: 0, y: 0,width: self.view.frame.size.width,height: self.view.frame.size.height)
        vc.labelTitle.text = title
        vc.arrayData = arrayData
        vc.status = status
        self.view.addSubview(vc.view)
        addChild(vc)
        vc.delegate = delegate
    }
    func showAlert(_ title:String,message:String,okTitle:String = "OK",delegate:AlertDelegate){
        
        let vc = AlertVC(nibName: "AlertVC", bundle: nil)
        vc.view.frame = CGRect(x: 0, y: 0,width: self.view.frame.size.width,height: self.view.frame.size.height)
        vc.labelMessage.text = message
        vc.labelTitle.text = title
        vc.delegate = delegate
        vc.buttonOK.setTitle(okTitle, for: .normal) 
        self.view.addSubview(vc.view)
        addChild(vc)
    }
    func showConfirmAlert(_ title:String,subTitle:String,yesTitle:String,noTitle:String,delegate:ConfirmAlertDelegate){
        
        let vc = ConfirmAlertVC(nibName: "ConfirmAlertVC", bundle: nil)
        vc.view.frame = CGRect(x: 0, y: 0,width: self.view.frame.size.width,height: self.view.frame.size.height)
        vc.setMessage(title, message: subTitle, yes: yesTitle, no: noTitle)
        vc.delegate = delegate
        self.view.addSubview(vc.view)
        addChild(vc)
    }
    func showNetworkError(){
        
    }
    func makeShadow(layer:CALayer){
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius = 6.0
        layer.shadowOpacity = 0.4
        layer.masksToBounds = false
    }
    func getCurrentDate(with:Int)->Int{
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        switch with {
        case 0:
            return components.year!
        case 1:
            return components.month!
        case 2:
            return components.day!
        default:
            return 0
        }
    }
    func callService(_ urlString: String, parameter: [String:Any] = [String: Any](),Method:HTTPMethod = .post, isWithLoading : Bool = true, dataKey: [String] = ["avatar"],data: [Data] = [Data](), isNeedToken: Bool = false, completionHandler: @escaping (ServiceResponse))
    {
        let url = urlString
        
        if NetworkReachabilityManager()!.isReachable
        {
            if isWithLoading
            {
                showLoading()
            }
            
            if data.count > 0
            {
                Alamofire.upload(
                    multipartFormData: { multipartFormData in
                        
                        for (key, value) in parameter
                        {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                        }
                        for i in 0..<data.count
                        {
                            multipartFormData.append(data[i], withName: dataKey[i], fileName: "file.jpg", mimeType: "image/png")
                        }
                },
                    to: url,
                    encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { result in
                                //print(result)
                                //print(result.result)
                                if let httpError = result.result.error
                                {
                                    print(NSString(data: result.data!, encoding: String.Encoding.utf8.rawValue)!)
                                    print(httpError._code)
                                    if isWithLoading
                                    {
                                        self.hideLoading()
                                    }
                                    self.showError(title:Messages.somthingWrong)
                                    //completionHandler(nil, httpError)
                                    
                                }
                                
                                if  result.result.isSuccess
                                {
                                    if let value = result.result.value {
                                        let myResponse1 = JSON(value)
                                        completionHandler(myResponse1,nil)
                                    }
                                }
                                
                                if isWithLoading
                                {
                                    self.hideLoading()
                                }
                                
                            }
                        case .failure(let encodingError):
                            print(encodingError)
                        }
                }
                )
            }
            else
            {
                var newUrl = url
                var params = parameter
                if Method == .get {
//                    newUrl = "\(url)&lang=\(Session.sharedInstance.getLanguageCode())"
                }else{
                    params["api_key"] = key
//                    params["lang"] = Session.sharedInstance.getLanguageCode()
                }
                print(params)
                Alamofire.request(newUrl, method:Method,parameters: params)
                    .responseJSON {  result in
                        //                        print(result)
                        //                        print(result.result)
                        if let httpError = result.result.error
                        {
                            print(NSString(data: result.data!, encoding: String.Encoding.utf8.rawValue)!)
                            print(httpError._code,NSURLErrorTimedOut)
                            if isWithLoading
                            {
                                self.hideLoading()
                            }
                            if httpError._code == NSURLErrorTimedOut {
                                print("Its timeout")
                            }
                            self.ShowTost(message: Messages.somthingWrong)
                            //completionHandler(nil, httpError )
                        }
                        
                        if  result.result.isSuccess
                        {
                            if let myResponse = result.result.value {
                                let json = JSON(myResponse)
                                completionHandler(json,nil)
                            }
                        }
                        
                        if isWithLoading
                        {
                            self.hideLoading()
                        }
                }
            }
        }
        else
        {
            self.ShowTost(message: Messages.internetNotAvailable)
        }
    }
}

