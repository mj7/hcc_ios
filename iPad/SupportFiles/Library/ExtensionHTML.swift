////
////  ExtensionHTML.swift
////  SLRB
////
////  Created by Fabri IT on 5/31/17.
////  Copyright © 2017 Fabri IT. All rights reserved.
////
//
//import UIKit
//import DTCoreText
//extension UIViewController : DTAttributedTextContentViewDelegate,DTLazyImageViewDelegate {
//    func getTextOption() -> [AnyHashable: Any] {
//        return [
//            DTDefaultLinkColor : UIColor(red: CGFloat(62.0 / 255), green: CGFloat(156.0 / 255), blue: CGFloat(153.0 / 255), alpha: CGFloat(1.0)),
//            // DTUseiOS6Attributes : true,
//            DTDefaultFontSize : 14.0,
//            DTDefaultTextAlignment : 0,
//            NSTextSizeMultiplierDocumentOption:1.0,
//            DTDefaultFontFamily : UIFont(name: "AvenirLTStd-Light", size: 14)?.familyName, //"Raleway-Light",
//            DTDefaultTextColor : "#595959"
//
//        ]
//    }
//    public func attributedTextContentView(_ attributedTextContentView: DTAttributedTextContentView, viewForLink url: URL, identifier: String, frame: CGRect) -> UIView? {
//        let button = DTLinkButton(frame: frame)
//        button.url = url
//        button.minimumHitSize = CGSize(width: CGFloat(25), height: CGFloat(25))
//        // adjusts it's bounds so that button is always large enough
//        button.guid = identifier
//        // use normal push action for opening URL
//        button.addTarget(self, action: #selector(self.linkPushed), for: .touchUpInside)
//        return button
//    }
//    public func attributedTextContentView(_ attributedTextContentView: DTAttributedTextContentView!, viewFor attachment: DTTextAttachment!, frame: CGRect) -> UIView! {
//        
//        if (attachment is DTImageTextAttachment) {
//            let imageView = DTLazyImageView(frame: frame)
//            imageView.delegate = self
//            imageView.contentView = attributedTextContentView
//            //            imageView.url = attachment.contentURL
//            return imageView
//        }
//
//        return nil;
//    }
//
//    @objc func linkPushed(_ button: DTLinkButton) {
//        UIApplication.shared.openURL((button.url?.absoluteURL)!)
//    }
//    func setHTML(text:String,htmlView:DTAttributedTextView) -> CGFloat {
//        let htmlText = text//"<font face='Raleway-Light' color='#333333'>" + text + "</font>"
//
//        let data = htmlText.data(using:String.Encoding.utf8)
//        let string = NSMutableAttributedString(htmlData: data, options: self.getTextOption(), documentAttributes: nil)
//
////        let paragraphStyle = NSMutableParagraphStyle()
////        paragraphStyle.lineSpacing = 12
////        paragraphStyle.alignment = .justified
////        string?.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, (string?.length)!))
//        htmlView.attributedString = string
//        htmlView.textDelegate = self
//
//        htmlView.sizeToFit()
//        let size = htmlView.attributedTextContentView.suggestedFrameSizeToFitEntireStringConstrainted(toWidth: htmlView.frame.size.width)
//        return size.height
//    }
//    func setHTML1(text:String,htmlView:DTAttributedLabel) -> CGFloat {
//        let htmlText = "<font face='Raleway-Light'>" + text + "</font>"
//
//        let data = htmlText.data(using:String.Encoding.utf8)
//        let string = NSAttributedString(htmlData: data, options: self.getTextOption(), documentAttributes: nil)
//        htmlView.attributedString = string
//        htmlView.delegate = self
//        htmlView.sizeToFit()
//
//        return 0
//    }
//}
