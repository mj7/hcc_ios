//
//  ExtensionImageView.swift
//  Alia Flower
//
//  Created by Pranav Goswami on 11/15/17.
//  Copyright © 2017 Pranav Goswami. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func setBorder() {
        self.layer.cornerRadius = 15
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1.0
        //        self.layer.borderColor = CGColor()
    }
    
}

extension UITableView {
    func setBackgoundLabel(message:String,color:UIColor = UIColor.black) {
        let bgLabel: UILabel     = UILabel(frame: CGRect(x:0,y: 0,width: self.bounds.size.width,height: self.bounds.size.height))
        bgLabel.text             = message
        bgLabel.textColor        = color
        bgLabel.font = UIFont(name: "OpenSans-Light", size: 15)
        bgLabel.numberOfLines    = 0
        bgLabel.textAlignment    = .center
        bgLabel.sizeToFit()
        self.backgroundView = bgLabel
    }
    
    func removeBackgoundLable() {
        self.backgroundView = nil
    }
}

extension UICollectionView {
    func setBackgoundLabel(message:String,color:UIColor = UIColor.black) {
        let bgLabel: UILabel     = UILabel(frame: CGRect(x:0,y: 0,width: self.bounds.size.width,height: self.bounds.size.height))
        bgLabel.text             = message
        bgLabel.textColor        = color
        bgLabel.font = UIFont(name: "OpenSans-Light", size: 15)
        bgLabel.numberOfLines    = 0
        bgLabel.textAlignment    = .center
        bgLabel.sizeToFit()
        self.backgroundView = bgLabel
    }
    
    func removeBackgoundLable() {
        self.backgroundView = nil
    }
}
extension UILabel
{
    
    func getLabelHight(text:String,font: UIFont, width:CGFloat) -> CGFloat
    {
        let label:UILabel = UILabel(frame: CGRect(x:0,y: 0,width: width,height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.textAlignment = .center
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
}
extension UIWebView {
    func loadUrl(url:String){
        self.loadRequest(URLRequest(url: URL(string:url)!))
    }
}
