//
//  DropDownVC.swift
//
//  Created by Fabri IT on 4/2/17.
//  Copyright © 2017 Fabri IT. All rights reserved.
//

import UIKit
@objc protocol DropDownDelegate {
    @objc optional func optionDidSelect(index:Int,status:Int)
}
class DropDownVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var labelTitle: UILabel!
    var arrayData = [DropDown]()
    let dropDownNib = "DropDownCell"
    var status:Int!
    @IBOutlet var tableViewData: UITableView!
    @IBOutlet weak var viewDropDown: UIView!
    
    @IBOutlet weak var constraintViewHeight: NSLayoutConstraint!
    var delegate:DropDownDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layer = viewDropDown.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius = 6.0
        layer.shadowOpacity = 0.4
        layer.masksToBounds = true
        viewDropDown.alpha = 0.25
        viewDropDown.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        tableViewData.register(UINib(nibName: dropDownNib,bundle: nil), forCellReuseIdentifier: dropDownNib)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        CATransaction.begin()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [.beginFromCurrentState, .curveLinear, .allowUserInteraction], animations: {
            self.viewDropDown.transform = CGAffineTransform.identity
        }, completion: nil)
        UIView.animate(withDuration: 0.15, delay: 0, options: [.beginFromCurrentState, .curveLinear, .allowUserInteraction], animations: {
            self.viewDropDown.alpha = 1
        }, completion: nil)
        CATransaction.commit()
    }
    @IBAction func buttonCloseClicked(_ sender: UIButton) {
        self.view.removeFromSuperview()
    }
    
    // MARK: - TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: dropDownNib, for: indexPath) as! DropDownCell
        
        let object = self.arrayData[(indexPath as NSIndexPath).row]
        cell.labelTitle.text = object.title
        //cell.imageViewIcon.image = object.icon
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.removeFromSuperview()
        delegate?.optionDidSelect!(index: indexPath.row,status: status)
        self.tableViewData.reloadData()
    }
}
