//
//  AlertVC.swift
//  VillaMamasiPhone
//
//  Created by Fabri IT on 6/1/16.
//  Copyright © 2016 Fabri IT. All rights reserved.
//

import UIKit
@objc protocol AlertDelegate {
    @objc optional func buttonOKClicked()
}
class AlertVC: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var buttonOK: UIButton!
    @IBOutlet weak var viewAlert: UIView!
    var animationTime = 0.6
    var vview:UIView!
    var delegate: AlertDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
//        if Session.sharedInstance.getLanguage() == "2" {
//            buttonOK.setTitle("حسنا", for: .normal)
//        }else{
//            buttonOK.setTitle("OK", for: .normal)
//        }
        
        vview = viewAlert
        let layer = vview.layer
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius = 6.0
        layer.shadowOpacity = 0.4
        layer.masksToBounds = false
        vview?.alpha = 0.25
        vview?.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
       
    }
    override func viewWillAppear(_ animated: Bool) {
        CATransaction.begin()
        //        CATransaction.setCompletionBlock {
        //            completion(true)
        //        }
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [.beginFromCurrentState, .curveLinear, .allowUserInteraction], animations: {
            self.vview?.transform = CGAffineTransform.identity
        }, completion: nil)
        UIView.animate(withDuration: 0.15, delay: 0, options: [.beginFromCurrentState, .curveLinear, .allowUserInteraction], animations: {
            self.vview?.alpha = 1
        }, completion: nil)
        CATransaction.commit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func buttonOKClicked(_ sender: AnyObject) {
        self.view.removeFromSuperview()
        if delegate != nil {
            delegate?.buttonOKClicked!()
        }
    }
    
    func setMessage(_ message:String) {
        self.labelMessage.text = message
    }

}
