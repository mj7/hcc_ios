

import UIKit
@objc protocol ConfirmAlertDelegate {
    @objc optional func buttonNoClicked()
    @objc optional func buttonYesClicked()
}
class ConfirmAlertVC: UIViewController {

    @IBOutlet var labelTitle: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var buttonNo: UIButton!
    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var buttonYes: UIButton!
    
    var delegate: ConfirmAlertDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
//            self.viewAlert.layoutIfNeeded()
//            self.viewAlert.layer.cornerRadius = 5
//            self.viewAlert.layer.masksToBounds = true
        
        let layer = viewAlert.layer
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius = 6.0
        layer.cornerRadius = 5
        layer.shadowOpacity = 0.4
        layer.masksToBounds = true
        layer.masksToBounds = false
        viewAlert?.alpha = 0.25
        viewAlert?.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        CATransaction.begin()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [.beginFromCurrentState, .curveLinear, .allowUserInteraction], animations: {
            self.viewAlert?.transform = CGAffineTransform.identity
        }, completion: nil)
        UIView.animate(withDuration: 0.15, delay: 0, options: [.beginFromCurrentState, .curveLinear, .allowUserInteraction], animations: {
            self.viewAlert?.alpha = 1
        }, completion: nil)
        CATransaction.commit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    @IBAction func buttonNoClicked(_ sender: AnyObject) {
        
        CATransaction.begin()
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [.beginFromCurrentState, .curveLinear, .allowUserInteraction], animations: {
            self.view.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
            self.view.alpha = 0
        }, completion:{(_) in
            self.view.removeFromSuperview()
        })
        CATransaction.commit()
        
        delegate?.buttonNoClicked!()
    }
    
    func setMessage(_ title:String,message:String, yes:String, no:String) {
        self.labelTitle.text = title
        self.labelMessage.text = message
        self.buttonNo.setTitle(no, for: .normal)
//        if Session.sharedInstance.getLanguage() == "2" {
//            self.buttonYes.setTitle("حسنا", for: .normal)
//        }else{
//            self.buttonYes.setTitle(yes,for: .normal)
//        }
    }
    
    @IBAction func buttonYesClicked(_ sender: AnyObject) {
        CATransaction.begin()
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [.beginFromCurrentState, .curveLinear, .allowUserInteraction], animations: {
            self.view.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
            self.view.alpha = 0
        }, completion:{(_) in
            self.view.removeFromSuperview()
        })
        CATransaction.commit()
        delegate?.buttonYesClicked!()
    }

}
