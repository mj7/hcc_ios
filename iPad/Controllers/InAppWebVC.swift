//
//  InAppWebVC.swift
//  FoodBoy
//
//  Created by Pranav Goswami on 1/25/18.
//  Copyright © 2018 Eashwar. All rights reserved.
//

import UIKit
import WebKit
class InAppWebVC: UIViewController,UIWebViewDelegate {

//    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var windowView: UIView!
    var urlString:String!
    var y:CGFloat = 84.0
    var delay = 0.3
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        let url = URL(string: urlString)!
        let request = URLRequest.init(url: url)
        webView.loadRequest(request)
        self.showLoading()
        windowView.alpha = 0
        y = windowView.frame.origin.y
        windowView.frame.origin.y = self.view.frame.size.height
        // Do any additional setup after loading the view.
        
    }
//    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
//        print(request.url?.absoluteString)
//        return true
//    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.hideLoading()
    }
    override func viewDidAppear(_ animated: Bool) {
        windowView.alpha = 1
//        UIView.animate(withDuration: 0.8) {
//            self.windowView.frame.origin.y = self.y
//        }
        UIView.animate(withDuration: 0.4, delay: delay, animations: {
            self.windowView.frame.origin.y = self.y
        }, completion: nil)
    }
    @IBAction func buttonCloseClicked(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2, animations: {
            self.windowView.frame.origin.y = self.view.frame.size.height
        }) { (Bool) in
            self.view.removeFromSuperview()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 

}
