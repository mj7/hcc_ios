//
//  ContactUsVC.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 21/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController {

    @IBOutlet weak var collectionViewData: UICollectionView!
    var arrayData = [ContactUs]()
    let identifier = "ContactUsCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        addArray()
        self.collectionViewData.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.flipImageViews(subviews: self.view.subviews)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func addArray(){
        var obj = ContactUs()
        obj.icon = #imageLiteral(resourceName: "map-pin")
        obj.title = "Location"
        obj.subTitle = "Building 11. Road No 3801. Manama. Bahrain"
        arrayData.append(obj)
        
        obj = ContactUs()
        obj.icon = #imageLiteral(resourceName: "MAIL-")
        obj.title = "Email"
        obj.subTitle = "info@gulfhotelbahrain.com"
        arrayData.append(obj)
        
        obj = ContactUs()
        obj.icon = #imageLiteral(resourceName: "CALL")
        obj.title = "Reception"
        obj.subTitle = "+973 17713000"
        arrayData.append(obj)
        
        obj = ContactUs()
        obj.icon = #imageLiteral(resourceName: "FAX")
        obj.title = "FAX"
        obj.subTitle = "+973 177113040"
        arrayData.append(obj)
        
        obj = ContactUs()
        obj.icon = #imageLiteral(resourceName: "HEART")
        obj.title = "Health Club"
        obj.subTitle = "+973 17713000"
        arrayData.append(obj)
        
        obj = ContactUs()
        obj.icon = #imageLiteral(resourceName: "SPA")
        obj.title = "Gulf Spa"
        obj.subTitle = "+973 17746289\n+973 17713000" //\nhttps://gulfhotelspa.com
        arrayData.append(obj)
        
        obj = ContactUs()
        obj.icon = #imageLiteral(resourceName: "CO")
        obj.title = "Gulf Convention Center"
        obj.subTitle = "+973 17713000"
        arrayData.append(obj)
        
        obj = ContactUs()
        obj.icon = #imageLiteral(resourceName: "PRESTIGE-CLUB")
        obj.title = "Prestige Club"
        obj.subTitle = "+973 17746284"
        arrayData.append(obj)
    }

}
//MARK: Collection Extension
extension ContactUsVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    
    
    func collectionView(_ collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAt indexPath:IndexPath) -> CGSize
    {
        
        let width = collectionViewData.frame.size.width / 4 - 10
        let cellSize:CGSize = CGSize(width: width, height: width - 25)
        return cellSize
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: identifier,for: indexPath) as! ContactUsCell
        let obj = arrayData[indexPath.row]

        cell.imageViewIcon.image = obj.icon
        cell.labelTitle.text = obj.title.uppercased()
        cell.labelSubTitle.text = obj.subTitle
        if indexPath.row == 1 {
            let font = UIFont(name: "AvenirLTStd-Book", size: 16.0)!
            cell.labelSubTitle.font = font
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = arrayData[indexPath.row]
        switch indexPath.row {
        case 0:
            self.openGoogleMap(query: "Gulf Hotel Bahrain")
            break
        case 1:
            self.sendEmail(mails: [obj.subTitle], subject: "", body: "")
            break
        case 2:
            self.call(phone: obj.subTitle.replacingOccurrences(of: " ", with: ""))
            break
        case 4:
            self.call(phone: obj.subTitle.replacingOccurrences(of: " ", with: ""))
            break
//        case 4:
//            self.call(phone: obj.subTitle.replacingOccurrences(of: " ", with: ""))
            
//            break
        case 5:
            self.call(phone: "+97317746289")
            break
        case 6:
            self.call(phone: obj.subTitle.replacingOccurrences(of: " ", with: ""))
            break
        case 7:
            self.call(phone: obj.subTitle.replacingOccurrences(of: " ", with: ""))
            break
        default:
            break
        }
    }
    
}
