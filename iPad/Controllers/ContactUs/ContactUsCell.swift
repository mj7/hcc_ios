//
//  ContactUsCell.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 21/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit

class ContactUsCell: UICollectionViewCell {

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelSubTitle: UILabel!
    @IBOutlet weak var imageViewIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
