//
//  LanguageSelectionVC.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 12/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
// hhhh

import UIKit
import MZDownloadManager
class LanguageSelectionVC: UIViewController {
    let myDownloadPath = MZUtility.baseFilePath + "/My_Downloads"
    lazy var downloadManager: MZDownloadManager = {
        [unowned self] in
        let sessionIdentifer: String = "com.iosDevelopment.MZDownloadManager.BackgroundSession"
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var completion = appDelegate.backgroundSessionCompletionHandler
        
        let downloadmanager = MZDownloadManager(session: sessionIdentifer, delegate: self, completion: completion)
        return downloadmanager
        }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
     }
    @IBAction func buttonRefreshClicked(_ sender: UIButton) {
        UserDefaults.standard.setValue("1", forKey: "update")
        UserDefaults.standard.synchronize()
        UIApplication.shared.windows[0].rootViewController = UIStoryboard(
            name: "Main",
            bundle: nil
            ).instantiateInitialViewController()
    }
    override func viewWillAppear(_ animated: Bool) {
       
    }
    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.bool(forKey: "languageChanged"){
            UserDefaults.standard.set(false, forKey: "languageChanged")
            UserDefaults.standard.synchronize()
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "CategoryVC", sender: self)
            }
            
        }else{
            let array = UserDefaults.standard.array(forKey: "imageArray") as! [String]
            print(array.count)
            for i in 0..<self.downloadManager.downloadingArray.count{
                self.downloadManager.cancelTaskAtIndex(i)
            }
            if self.downloadManager.downloadingArray.count == 0 {
                for object in array {
                    let obj = "\(object)"
                    let theFileName = (obj as NSString).lastPathComponent
                    let myDownloadPath = MZUtility.baseFilePath + "/My_Downloads"
                    let url = myDownloadPath + "/" + theFileName
                    if !FileManager.default.fileExists(atPath: url){
                        self.downloadManager.addDownloadTask(theFileName, fileURL: obj, destinationPath: myDownloadPath)
                    }
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonInfoClicked(_ sender: UIButton) {
        let info = "Mawasim Restaurant & Lounge has made every effort to ensure that the allergen information provided is accurate. However, because of the handcrafted nature of our menu items, the variety of procedures used in our kitchens, shared cooking and preparation areas including common fryer oil and our reliance on our suppliers, we cannot make any guarantees as to its accuracy and disclaim liability for its use. The material is provided for informational purposes only and is not meant as a substitute for advice provided by a healthcare professional.\n\nIf you have food allergies, please be aware that we use milk, eggs, tree nuts,peanuts, seafood, soy, wheat, crustacean shellfish, fish and other allergens in our kitchens. All items are prepared in common areas and may come in contact with or traces of these products and other potential allergens or ingredients."
        self.showAlert("NUTRITION & ALLERGEN NOTICE", message: info,okTitle: "GO TO MENU",delegate: self)
    }
    @IBAction func buttonEnglishClicked(_ sender: UIButton) {
        Session.sharedInstance.setLanguage(val: "1")
        Session.sharedInstance.setAppleLAnguageTo("en")
        self.loadConstraint()
//        self.performSegue(withIdentifier: "HomeVC", sender: self)
        UserDefaults.standard.set(true, forKey: "languageChanged")
        UserDefaults.standard.synchronize()
        UIApplication.shared.windows[0].rootViewController = UIStoryboard(
            name: "Main",
            bundle: nil
            ).instantiateInitialViewController()
    }
    
    @IBAction func buttonArabicClicked(_ sender: UIButton) {
        Session.sharedInstance.setLanguage(val: "2")
        Session.sharedInstance.setAppleLAnguageTo("ar")
//        Session.sharedInstance.DoTheSwizzling()
        self.loadConstraint()
        
        UserDefaults.standard.set(true, forKey: "languageChanged")
        UserDefaults.standard.synchronize()
        
        UIApplication.shared.windows[0].rootViewController = UIStoryboard(
            name: "Main",
            bundle: nil
            ).instantiateInitialViewController()
//        self.performSegue(withIdentifier: "HomeVC", sender: self)
    }
    
    @IBAction func buttonChangeLanugageClicked(_ sender: UIButton) {
        var lan = "arabic"
        if Session.sharedInstance.getLanguage() == "2" {
            lan = "english"
        }
        self.showConfirmAlert("ARE YOU SURE?", subTitle: "Are you sure you want to change your language to \(lan)?", yesTitle: "YES", noTitle: "NO", delegate: self)
    }
    
    
    @IBAction func buttonMenuClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "CategoryVC", sender: self)
    }
}
extension LanguageSelectionVC : MZDownloadManagerDelegate {
    func downloadRequestStarted(_ downloadModel: MZDownloadModel, index: Int) {
        
        
    }
    
    func downloadRequestDidPopulatedInterruptedTasks(_ downloadModels: [MZDownloadModel]) {
        
    }
    
    func downloadRequestDidUpdateProgress(_ downloadModel: MZDownloadModel, index: Int) {
        
    }
    
    func downloadRequestDidPaused(_ downloadModel: MZDownloadModel, index: Int) {
        
    }
    
    func downloadRequestDidResumed(_ downloadModel: MZDownloadModel, index: Int) {
        
    }
    
    func downloadRequestCanceled(_ downloadModel: MZDownloadModel, index: Int) {
        
        print("Cancelled")
    }
    
    func downloadRequestFinished(_ downloadModel: MZDownloadModel, index: Int) {
        //         self.index += 1
        //        downloadImage()
        print("Done")
    }
    
    func downloadRequestDidFailedWithError(_ error: NSError, downloadModel: MZDownloadModel, index: Int) {
        debugPrint("Error while downloading file: \(downloadModel.fileName)  Error: \(error)")
    }
    
    //Oppotunity to handle destination does not exists error
    //This delegate will be called on the session queue so handle it appropriately
    func downloadRequestDestinationDoestNotExists(_ downloadModel: MZDownloadModel, index: Int, location: URL) {
        let myDownloadPath = MZUtility.baseFilePath + "/My_Downloads"
        if !FileManager.default.fileExists(atPath: myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        let fileName = MZUtility.getUniqueFileNameWithPath((myDownloadPath as NSString).appendingPathComponent(downloadModel.fileName as String) as NSString)
        let path =  myDownloadPath + "/" + (fileName as String)
        try! FileManager.default.moveItem(at: location, to: URL(fileURLWithPath: path))
        debugPrint("Default folder path: \(myDownloadPath)")
    }
}

extension LanguageSelectionVC:AlertDelegate {
    func buttonOKClicked() {
        self.performSegue(withIdentifier: "CategoryVC", sender: self)
    }
}

extension LanguageSelectionVC:ConfirmAlertDelegate{
    func buttonYesClicked() {
        if Session.sharedInstance.getLanguage() == "1" {
            buttonArabicClicked(UIButton())
        }else{
            buttonEnglishClicked(UIButton())
        }
    }
    func buttonNoClicked() {
        
    }
}
