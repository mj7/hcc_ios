//
//  FilterCell.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 22/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit

class FilterCell: UICollectionViewCell {

    @IBOutlet weak var buttonRadio: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
