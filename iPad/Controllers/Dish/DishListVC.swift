//
//  DishListVC.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 13/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit
import MZDownloadManager
class DishListVC: UIViewController {
    
    @IBOutlet weak var labelNavigationTitle: UILabel!
    @IBOutlet weak var collectionViewProduct: UICollectionView!
    
    @IBOutlet weak var viewCategoryList: UIView!
    
    @IBOutlet weak var viewFilters: UIView!
    @IBOutlet weak var collectionViewFilter: UICollectionView!
    
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var labelDiscover: UILabel!
    
    
    @IBOutlet weak var labelRestaurantName: UILabel!
    @IBOutlet weak var viewNoData: UIView!
    
    
    let myDownloadPath = MZUtility.baseFilePath + "/My_Downloads"
    
    
    let identifierDish = "ProductCell"
    let identifierTag = "FilterCell"
    let identifierCategory = "CategoryTableCell"
    var selectedRestaurant:RestaurantDB!
    var selectedParentCategory:CategoryDB!
    
    var selectedDish:DishDB!
    var arrayData = [DishDB]()
    var arrayQTY = [Int]()
    var selectedCategoryIndex = 0
    var alert:AddToCartAlert!
    var arrayTags = [TagDB]()
    var arraySelectedTags = [TagDB]()
     @IBOutlet weak var lblCart: UILabel!{
         didSet{
             if Session.sharedInstance.getLanguage() == "1" {
                 lblCart.text = "Cart"
             }else{
                 lblCart.text = "عربة التسوق"
             }
         }
     }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionViewFilter.register(UINib(nibName: identifierTag, bundle: nil), forCellWithReuseIdentifier: identifierTag)
        self.collectionViewProduct.register(UINib(nibName: identifierDish, bundle: nil), forCellWithReuseIdentifier: identifierDish)
        
        labelNavigationTitle.text = isEnglish() ? selectedParentCategory.name : selectedParentCategory.name_ar
        labelDiscover.text = "Discover something new.\nWhat would you like to eat?"
//        labelRestaurantName.text = isEnglish() ? selectedRestaurant.name : selectedRestaurant.name_ar
        reloadDishView()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.flipImageViews(subviews: self.view.subviews)
    }
    func reloadDishView(){
        arrayData = self.fetchData("DishDB", predicate: "categoryID == %@", id: selectedParentCategory.id!) as! [DishDB]
        reloadData()
    }
    func reloadData(){
        arrayTags.removeAll()
        for obj in arrayData {
           // print(obj.tags?.count)
            for tag in obj.tags?.allObjects as! [TagDB] {
                if !arrayTags.contains(tag){
                    arrayTags.append(tag)
                }
            }
        }
        //print(arrayTags.count)
        if arraySelectedTags.count > 0 {
            var tempArray = [DishDB]()
            for obj in arrayData {
                var found = false
                for tag in obj.tags?.allObjects as! [TagDB] {
                    if arraySelectedTags.contains(tag){
                        found = true
                    }
                }
                if found{
                    tempArray.append(obj)
                }
            }
            arrayData = tempArray
        }
        arrayQTY.removeAll()
        for _ in arrayData {
            arrayQTY.append(1)
        }
        collectionViewProduct.reloadData()
        collectionViewFilter.reloadData()
        if arrayData.count  == 0 {
            viewNoData.isHidden = false
        }else{
            viewNoData.isHidden = true
        }
    }
    override func viewDidAppear(_ animated: Bool) {

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DishDetailVC" {
            let vc = segue.destination as! DishDetailVC
//            vc.selectedRestaurant = selectedRestaurant
            vc.selectedDish = selectedDish
        }
    }
    
    
    
    
    @IBAction func buttonCloseClicked(_ sender: UIButton) {
        self.viewSearch.isHidden = true
    }
    
    @IBAction func buttonFindClicked(_ sender: UIButton) {
        textFieldSearch.resignFirstResponder()
        
        let predicate = NSPredicate(format: "categoryID == %@ and (name contains[c] %@ || name_ar contains[c] %@)", selectedParentCategory.id!, textFieldSearch.text!, textFieldSearch.text!)
        arrayData = self.fetchData("DishDB", predicate:predicate) as! [DishDB]
        self.viewSearch.isHidden = true
        reloadData()
        
    }
    
    @IBAction func buttonSearchClicked(_ sender: UIButton) {
        reloadDishView()
        textFieldSearch.text = ""
        self.viewSearch.isHidden = false
        if alert != nil && alert.frame.origin.x != 0 {
            self.alert.removeFromSuperview()
        }
    }
    
    @IBAction func buttonFilterClicked(_ sender: UIButton) {
        self.viewFilters.isHidden = false
        if alert != nil && alert.frame.origin.x != 0 {
            self.alert.removeFromSuperview()
        }
    }
    
    @IBAction func buttonFilterCloseClicked(_ sender: UIButton) {
        self.viewFilters.isHidden = true
        
        
    }
    
    @IBAction func buttonClearAllClicked(_ sender: UIButton) {
        arraySelectedTags.removeAll()
        reloadDishView()
        self.viewFilters.isHidden = true
    }
    
    @IBAction func buttonApplyFilterClicked(_ sender: UIButton) {
        reloadDishView()
        self.viewFilters.isHidden = true
    }
    @IBAction func buttonCloseCategoryClicked(_ sender: UIButton) {
        self.viewCategoryList.isHidden = true
        
    }
    
}
//MARK: Collection Extension
extension DishListVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewFilter {
            return arrayTags.count
        }
        return arrayData.count
    }
    
    
    
    func collectionView(_ collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAt indexPath:IndexPath) -> CGSize
    {
        if collectionView == collectionViewFilter {
            return CGSize(width: self.collectionViewFilter.frame.width / 2 - 10, height: 55)
        }
        
        let width = collectionViewProduct.frame.size.width / 3 - 10
        let height = 320/300 * width
        let cellSize:CGSize = CGSize(width: width, height: height)
        return cellSize
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewFilter {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: identifierTag,for: indexPath) as! FilterCell
            let obj = arrayTags[indexPath.row]
            cell.labelTitle.text = isEnglish() ? obj.name!.uppercased() : obj.name_ar!
            if arraySelectedTags.contains(obj){
                cell.buttonRadio.isSelected = true
                cell.labelTitle.textColor = UIColor.black
            }else{
                cell.buttonRadio.isSelected = false
                cell.labelTitle.textColor = #colorLiteral(red: 0.5027191639, green: 0.5027315021, blue: 0.5027248263, alpha: 1)
            }
            
            return cell
        }
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: identifierDish,for: indexPath) as! ProductCell
        let obj = arrayData[indexPath.row]
        cell.labelTitle.text = isEnglish() ? obj.name! : obj.name_ar!
        cell.labelSubTitle.text = isEnglish() ? obj.price: obj.price_ar!
       // print(obj.price)
        cell.imageViewLogo.setOfflineImage(imageName: obj.main_image!)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewFilter {
            let obj = arrayTags[indexPath.row]
            var found = false
            for i in 0 ..< arraySelectedTags.count{
                let tag = arraySelectedTags[i]
                if obj.id! == tag.id! {
                    found = true
                    arraySelectedTags.remove(at: i)
                    break
                }
            }
            if !found{
                arraySelectedTags.append(obj)
            }
            collectionViewFilter.reloadData()
        }else{
            selectedDish = arrayData[indexPath.row]
            self.performSegue(withIdentifier: "DishDetailVC", sender: self)
        }
        
    }
}
