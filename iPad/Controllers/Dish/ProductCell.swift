//
//  RestaurantCell.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 13/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {

    @IBOutlet weak var imageViewLogo: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelSubTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
