//
//  CategoryTableCell.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 27/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit

class CategoryTableCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
