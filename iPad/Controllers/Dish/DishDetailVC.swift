//
//  DishDetailVC.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 20/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit
import MZDownloadManager
class DishDetailVC: UIViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDescriptions: UILabel!
    @IBOutlet weak var labelQTY: UILabel!
    @IBOutlet weak var labelNavigationTitle: UILabel!
    @IBOutlet weak var imageViewDish: UIImageView!
    @IBOutlet weak var lblCart: UILabel!{
        didSet{
            if Session.sharedInstance.getLanguage() == "1" {
                lblCart.text = "Cart"
            }else{
                lblCart.text = "عربة التسوق"
            }
        }
    }
    
    

    var selectedDish:DishDB!
    let identifier = "TagCell"
    var arrayImage = [Source]()

    override func viewDidLoad() {
        super.viewDidLoad()

        labelNavigationTitle.text = isEnglish() ? selectedDish.name : selectedDish.name_ar
        labelName.text = "\(isEnglish() ? selectedDish.name! : selectedDish.name_ar!) - \(String(describing: isEnglish() ? selectedDish.price! : selectedDish.price_ar!))"
        labelDescriptions.text = isEnglish() ? selectedDish.detail!:selectedDish.detail_ar
//        labelPrice.text = isEnglish() ? selectedDish.price! : selectedDish.price_ar
        labelQTY.text = "1"
        let array = self.fetchData("GalleryDB", predicate: "ANY dish == %@", obj: selectedDish) as! [GalleryDB]
        if array.count > 0 {
            let obj = array[0]
            imageViewDish.setOfflineImage(imageName: obj.url!)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.flipImageViews(subviews: self.view.subviews)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func buttonPlusClicked(_ sender: UIButton) {
        var qty:Int = Int(labelQTY.text!)!
        qty += 1
        labelQTY.text = "\(qty)"
    }
    
    @IBAction func buttonMinusClicked(_ sender: Any) {
        var qty:Int = Int(labelQTY.text!)!
        if qty > 1 {
            qty -= 1
        }
        labelQTY.text = "\(qty)"
    }
    
    @IBAction func buttonAddToCartClicked(_ sender: UIButton) {
//        if self.canAddDish(id: selectedRestaurant.id!) {
            let object = selectedDish
            let context = self.getContext()
            let cart = self.fetchAllData("CartDB") as! [CartDB]
            var found = false
            for dish in cart {
                if dish.dish == object {
                    dish.qty = dish.qty + Int16(labelQTY.text!)!
                    found = true
                }
            }
            if !found {
                let obj = CartDB(context: context)
                obj.qty = Int16(labelQTY.text!)!
                obj.dish = object
            }
            self.saveContext()
            UserDefaults.standard.setValue("1", forKey: "alert")
            UserDefaults.standard.synchronize()
            self.goBack(UIButton())
//        }else{
//            let alertController = UIAlertController(title: "Dish can not be added", message: "You alreay have dish from diffirent restaurant. Please clear cart.", preferredStyle: .alert)
//
//            let action2 = UIAlertAction(title: "OK", style: .cancel) { (action:UIAlertAction) in
//
//            }
//            alertController.addAction(action2)
//            self.present(alertController, animated: true, completion: nil)
//        }
    }
}
