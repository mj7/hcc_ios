//
//  RestuarantListVC.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 13/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit

class RestaurantListVC: UIViewController {
    
    @IBOutlet weak var collectionViewCategory: UICollectionView!
    @IBOutlet weak var collectionViewData: UICollectionView!
    
    
    var arrayData = [RestaurantDB]()
    var arrayTypes = [TypeDB]()
    let identifier = "RestaurantCell"
    let identifierType = "TypeCell"
    var selectedRestaurant:RestaurantDB!
    var selectedType:TypeDB!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionViewData.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
        self.collectionViewCategory.register(UINib(nibName: identifierType, bundle: nil), forCellWithReuseIdentifier: identifierType)
//        getRestaurant()
        arrayTypes = self.fetchAllData("TypeDB") as! [TypeDB]
        selectedType = arrayTypes[0]
        reloadData()
//        collectionViewData.reloadData()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.flipImageViews(subviews: self.view.subviews)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadData(){
        arrayData = self.fetchData("RestaurantDB", predicate: "ANY type == %@", obj: selectedType) as! [RestaurantDB]
        collectionViewData.reloadData()
        collectionViewCategory.reloadData()
    }
    
     // MARK: - Navigation
     
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RestaurantDetailVC" {
            let vc = segue.destination as! RestaurantDetailVC
            vc.restaurant = selectedRestaurant
        }
     }
 
    
}
//MARK: Collection Extension
extension RestaurantListVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewCategory {
            return arrayTypes.count
        }
        
        return arrayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == collectionViewCategory {
            var width = 0
            for obj in arrayTypes {
                let font = UIFont(name: "AvenirLTStd-Book", size: 16)
                let w:CGFloat = self.getWidthForView(isEnglish() ? obj.name!.uppercased():obj.name_ar!, font: font!, height: 21)
                width += Int(w) + 70
            }
            let space = (Int(collectionViewCategory.frame.width) - width) / 2
            return UIEdgeInsets(top: 0,left: CGFloat(space),bottom: 0,right: CGFloat(space))//CGFloat(space)
            
        }
        return UIEdgeInsets(top: 0,left: 0,bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAt indexPath:IndexPath) -> CGSize
    {
        if collectionView == collectionViewCategory {
            let obj = arrayTypes[indexPath.row]
            let font = UIFont(name: "AvenirLTStd-Book", size: 16)
            let width:CGFloat = self.getWidthForView(isEnglish() ? obj.name!.uppercased():obj.name_ar!, font: font!, height: 21)
            let height:CGFloat = 40
            let cellSize:CGSize = CGSize(width: width + 60, height: height)
            
            return cellSize
          
        }
        
        let width = collectionViewData.frame.size.width / 3 - 15
        let cellSize:CGSize = CGSize(width: width, height: width)
        return cellSize
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewCategory {
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: identifierType,for: indexPath) as! TypeCell
            let obj = arrayTypes[indexPath.row]
            cell.labelTitle.text = isEnglish() ? obj.name!.uppercased():obj.name_ar!
            if obj == selectedType {
                cell.viewIndicator.borderWidth = 0
                cell.viewIndicator.backgroundColor = appColor
            }else{
                cell.viewIndicator.backgroundColor = UIColor.clear
                cell.viewIndicator.borderWidth = 1
            }
            return cell
        
        }
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: identifier,for: indexPath) as! RestaurantCell
        let obj = arrayData[indexPath.row]
        
        
        cell.imageViewLogo.setOfflineImage(imageName: obj.background!)
        
        cell.labelTitle.text = isEnglish() ? obj.name!.uppercased():obj.name_ar!
        cell.labelSubTitle.text = isEnglish() ? obj.cuisine!:obj.cuisine_ar
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewData {
            selectedRestaurant = arrayData[indexPath.row]
            self.performSegue(withIdentifier: "RestaurantDetailVC", sender: self)
        }else{
            selectedType = arrayTypes[indexPath.row]
            reloadData()
        }
    }
    
}

