//
//  RestaurantDetailVC.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 13/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit

class RestaurantDetailVC: UIViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelCuisine: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var imageViewLogo: UIImageView!
    @IBOutlet weak var imageViewBackground: UIImageView!
    
    var restaurant:RestaurantDB!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelName.text = isEnglish() ? restaurant.name: restaurant.name_ar
        labelCuisine.text = isEnglish() ? restaurant.cuisine:restaurant.cuisine_ar
        
        labelDescription.text = isEnglish() ? restaurant.detail:restaurant.detail_ar
        labelDescription.setLineSpacing(lineSpacing: 6)
        labelPhone.text =  restaurant.phone
        imageViewLogo.setOfflineImage(imageName: restaurant.logo!)
        imageViewBackground.setOfflineImage(imageName: restaurant.details_background_image!)
        
        
      
    }
    override func viewWillAppear(_ animated: Bool) {
        self.flipImageViews(subviews: self.view.subviews)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func buttonMenuClicked(_ sender: UIButton) {
      //  let array = self.fetchData("CategoryDB", predicate: "restaurantID == %@", id: restaurant.id!) as! [CategoryDB]
        
//        self.performSegue(withIdentifier: "DishListVC", sender: self)
        self.performSegue(withIdentifier: "CategoryVC", sender: self)
    }
    @IBAction func buttonSpecialOffersClicked(_ sender: UIButton) {
        
    }
    @IBAction func buttonRefreshDataClicked(_ sender: UIButton) {
        UserDefaults.standard.setValue("1", forKey: "update")
        UserDefaults.standard.synchronize()
        UIApplication.shared.windows[0].rootViewController = UIStoryboard(
            name: "Main",
            bundle: nil
            ).instantiateInitialViewController()
    }
    @IBAction func buttonBookATableClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "BookATableVC", sender: self)
    }
    
    @IBAction func buttonCallClicked(_ sender: UIButton) {
        self.call(phone: restaurant.phone!)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CategoryVC" {
            let vc = segue.destination as! CategoryVC
            
        }else if segue.identifier == "BookATableVC" {
            let vc = segue.destination as! BookATableVC
            vc.selectedRestaurantFromDetail = restaurant
            
        }
    }

}
extension UILabel {
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {
        
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        
        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
//        // (Swift 4.2 and above) Line spacing attribute
//        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        
        // (Swift 4.1 and 4.0) Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
}
