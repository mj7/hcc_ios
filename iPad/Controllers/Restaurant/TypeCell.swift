//
//  CategoryCell.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 13/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit

class TypeCell: UICollectionViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewIndicator: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
