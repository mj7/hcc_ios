//
//  CategoryVC.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 20/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit

class CategoryVC: UIViewController {

    @IBOutlet weak var labelNavigationTitle: UILabel!
    @IBOutlet weak var collectionViewData: UICollectionView!
    let identifier = "RestaurantCell"
    var arrayData = [CategoryDB]()
    var selectedCategory:CategoryDB!
    @IBOutlet weak var lblCart: UILabel!{
        didSet{
            if Session.sharedInstance.getLanguage() == "1" {
                lblCart.text = "Cart"
            }else{
                lblCart.text = "عربة التسوق"
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let array = self.fetchAllData("RestaurantDB") as! [RestaurantDB]
//        selectedRestaurant = array[0]
        self.collectionViewData.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
        if selectedCategory == nil {
            arrayData = self.fetchData("CategoryDB", predicate: "parentId == %@", id: "0") as! [CategoryDB]
        }else{
            labelNavigationTitle.text = isEnglish() ? selectedCategory.name : selectedCategory.name_ar
            arrayData = self.fetchData("CategoryDB", predicate: "parentId == %@", id: selectedCategory.id!) as! [CategoryDB]
        }
        UIApplication.shared.statusBarStyle = .default
    }
    override func viewWillAppear(_ animated: Bool) {
        self.flipImageViews(subviews: self.view.subviews)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DishListVC" {
            let vc = segue.destination as! DishListVC
            vc.selectedParentCategory = selectedCategory
        }
    }
}
//MARK: Collection Extension
extension CategoryVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayData.count
    }
    
    
    
    func collectionView(_ collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAt indexPath:IndexPath) -> CGSize
    {
        
        let width = collectionViewData.frame.size.width / 4 - 10
        let height = 290/225 * width
        let cellSize:CGSize = CGSize(width: width, height: height)
        return cellSize
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: identifier,for: indexPath) as! RestaurantCell
        let obj = arrayData[indexPath.row]
        
        cell.imageViewLogo.setOfflineImage(imageName: obj.logo!)
        cell.labelTitle.text = isEnglish() ? obj.name?.uppercased() : obj.name_ar
        cell.labelSubTitle.text = ""
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCategory = arrayData[indexPath.row]
        let temp = self.fetchData("CategoryDB", predicate: "parentId == %@", id: selectedCategory.id!) as! [CategoryDB]
        if temp.count == 0 {
            self.performSegue(withIdentifier: "DishListVC", sender: self)
        }else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
            vc.selectedCategory = selectedCategory
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
