//
//  BookRestaurantCell.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 25/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit

class BookRestaurantCell: UICollectionViewCell {

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelSubTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
