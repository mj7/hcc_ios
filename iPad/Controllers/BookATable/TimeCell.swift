//
//  TimeCell.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 29/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit

class TimeCell: UITableViewCell {

    @IBOutlet weak var viewIndicator: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
