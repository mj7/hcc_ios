//
//  BookATableVC.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 25/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import SkyFloatingLabelTextField
class BookATableVC: UIViewController {
    @IBOutlet weak var imageViewChooseRestaurant: UIImageView!
    @IBOutlet weak var imageViewReservationDetails: UIImageView!
    @IBOutlet weak var imageViewContactDetails: UIImageView!
    
    @IBOutlet weak var collectionViewRestaurantList: UICollectionView!
    @IBOutlet weak var viewReservationDetails: UIView!
    @IBOutlet weak var viewContactDetails: UIView!
    
    
    @IBOutlet weak var viewIndicatorChooseRestaurant: UIView!
    @IBOutlet weak var viewIndicatorReservationDetails: UIView!
    @IBOutlet weak var constraintChooseRestaurant: NSLayoutConstraint!
    @IBOutlet weak var constraintReservationDetails: NSLayoutConstraint!
    
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTimeHour: UILabel!
    @IBOutlet weak var labelTimeMinutes: UILabel!
    @IBOutlet weak var labelNumberOfPerson: UILabel!
    @IBOutlet weak var textFieldSpecialRequest: KMPlaceholderTextView!
    
    @IBOutlet weak var tableViewTimeHours: UITableView!
    @IBOutlet weak var tableViewTimeMinutes: UITableView!
    @IBOutlet weak var tableViewNoOfPersons: UITableView!
    
    @IBOutlet weak var viewTimeHours: UIView!
    @IBOutlet weak var viewTimeMinutes: UIView!
    @IBOutlet weak var viewNoOfPersons: UIView!
    
    
    @IBOutlet weak var textFieldFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldPhone: SkyFloatingLabelTextField!
    
    
    @IBOutlet weak var viewSucess: UIView!
    @IBOutlet weak var labelSuccessMessage: UILabel!
    
    var arrayRestaurant = [Restaurant]()
    var selectedRestaurant:Restaurant!
    var selectedRestaurantFromDetail:RestaurantDB!
    var step = 1
    let identifier = "BookRestaurantCell"
    let identifierDropDown = "TimeCell"
    var datePicker = DatePicker.getFromNib()
    var arrayHours = [String]()
    var arrayMinutes = [String]()
    var arrayPersons = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionViewRestaurantList.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
        tableViewTimeHours.register(UINib(nibName: identifierDropDown,bundle: nil), forCellReuseIdentifier: identifierDropDown)
        tableViewTimeMinutes.register(UINib(nibName: identifierDropDown,bundle: nil), forCellReuseIdentifier: identifierDropDown)
        tableViewNoOfPersons.register(UINib(nibName: identifierDropDown,bundle: nil), forCellReuseIdentifier: identifierDropDown)
        
        getRestaurant()
        setupDatePicker()
        for i in 0..<24 {
            if i < 10 {
                arrayHours.append("0\(i)")
            }else{
                arrayHours.append("\(i)")
            }
        }
        for i in 0..<16 {
            if i < 10 {
                arrayPersons.append("0\(i)")
            }else{
                arrayPersons.append("\(i)")
            }
        }
        for i in 0..<60 {
            if i < 10 {
                arrayMinutes.append("0\(i)")
            }else{
                arrayMinutes.append("\(i)")
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.flipImageViews(subviews: self.view.subviews)
    }
    override func viewDidAppear(_ animated: Bool) {
        constraintChooseRestaurant.constant = viewIndicatorChooseRestaurant.frame.size.width
        constraintReservationDetails.constant = viewIndicatorReservationDetails.frame.size.width
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func buttonContinueClicked(_ sender: UIButton) {
        if step == 1 {
            collectionViewRestaurantList.isHidden = true
            viewReservationDetails.isHidden = false
            imageViewChooseRestaurant.image = #imageLiteral(resourceName: "rounded_tic_dark")
            step = 2
            
        }else if step == 2 {
            viewReservationDetails.isHidden = true
            viewContactDetails.isHidden = false
            
            step = 3
            
            UIView.animate(withDuration: 2, animations: {
                self.constraintChooseRestaurant.constant = 0
                self.view.layoutIfNeeded()
            }) { (bool) in
                self.imageViewReservationDetails.image = #imageLiteral(resourceName: "rounded_tic_dark")
            }
            
        }else{
            
            if textFieldFirstName.text == "" {
                textFieldFirstName.errorMessage = textFieldFirstName.placeholder
            }else if textFieldLastName.text == "" {
                textFieldLastName.errorMessage = textFieldLastName.placeholder
            }else if textFieldEmail.text == "" {
                textFieldEmail.errorMessage = textFieldEmail.placeholder
            }else if textFieldPhone.text == "" {
                textFieldPhone.errorMessage = textFieldPhone.placeholder
            }else{
                bookATable()
                UIView.animate(withDuration: 2, animations: {
                    self.constraintReservationDetails.constant = 0
                    self.view.layoutIfNeeded()
                }) { (bool) in
                    self.imageViewContactDetails.image = #imageLiteral(resourceName: "rounded_tic_dark")
                }
            }
        }
    }
    
    @IBAction func buttonDateClicked(_ sender: UIButton) {
        self.datePicker.show(inVC: self)
    }
    @IBAction func buttonTimeClicked(_ sender: UIButton) {
        hideAllDropDown()
        if sender.tag == 1 {
            viewTimeHours.isHidden = false
        }else{
            viewTimeMinutes.isHidden = false
        }
    }
    @IBAction func buttonCallClicked(_ sender: UIButton) {
        self.call(phone: "+97317713000")
    }
    @IBAction func buttonNumberOfPersonsClicked(_ sender: UIButton) {
        hideAllDropDown()
        viewNoOfPersons.isHidden = false
    }
    func setupDatePicker() {
        datePicker.delegate = self
        var date = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        datePicker.config.startDate = date
        let d = self.getStringFromDate(date: date!, inFormate: "yyyy-MM-dd")
        self.labelDate.text = d
        datePicker.datePicker.minimumDate = date
        date = Calendar.current.date(byAdding: .month, value: 1, to: Date())
        datePicker.datePicker.maximumDate = date
        datePicker.config.animationDuration = 0.25
        datePicker.config.cancelButtonTitle = "Cancel"
        datePicker.config.confirmButtonTitle = "Confirm"
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        datePicker.config.confirmButtonColor = appColor
        datePicker.config.cancelButtonColor = appColor
    }
    
}
extension BookATableVC:DatePickerDelegate {
    
    func datePicker(_ amDatePicker: DatePicker, didSelect date: Date) {
        let d = self.getStringFromDate(date: date, inFormate: "yyyy-MM-dd")
        //            self.selectedDate = self.getStringFromDate(date: date, inFormate: "dd-MM-yyyy")
        //            self.labelDate.text = self.selectedDate
        self.labelDate.text = d
    }
    
    func datePickerDidCancelSelection(_ amDatePicker: DatePicker) {
        
    }
    
    
}
extension BookATableVC {
    func getRestaurant() {
        let param = ["":""]
        callService(Path.getRestaurant, parameter: param,Method:.post) { (json, error) in
            print(json)
            
            for result in json["result"].arrayValue {
                for object in result["restaurants"].arrayValue {
                    let obj = Restaurant.init(fromJson: object)
                    if obj.id == self.selectedRestaurantFromDetail.id {
                        self.selectedRestaurant = obj
                    }
                    self.arrayRestaurant.append(obj)
                }
            }
            self.collectionViewRestaurantList.reloadData()
            print(self.arrayRestaurant.count)
        }
    }
    
    func bookATable() {
        let param = [
            "restaurant_id": selectedRestaurant.id,
            "number_guest" : labelNumberOfPerson.text!,
            "date_booking" : labelDate.text!,
            "booking_time" : "\(labelTimeHour.text!):\(labelTimeMinutes.text!)",
            "booking_notes":textFieldSpecialRequest.text!,
            "first_name" : textFieldFirstName.text!,
            "last_name" : textFieldFirstName.text!,
            "email" : textFieldFirstName.text!,
            "mobile" : textFieldFirstName.text!,
            
        ]
        
        callService(Path.bookARestaurant, parameter: param,Method:.post) { (json, error) in
            print(json)
            var message = json["result"]["message_en"].stringValue
            if !self.isEnglish() {
                message = json["result"]["message_ar"].stringValue
            }
            if json["status"].stringValue == "Successful" {
                self.labelSuccessMessage.text = message
                self.viewSucess.isHidden = false
            }else{
                
                self.showError(title: message )
            }
            
        }
    }
}
//MARK: Collection Extension
extension BookATableVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrayRestaurant.count
    }
    
    
    func collectionView(_ collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAt indexPath:IndexPath) -> CGSize
    {
        
        let width = collectionViewRestaurantList.frame.size.width / 5 - 5
        let cellSize:CGSize = CGSize(width: width, height: width)
        return cellSize
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: identifier,for: indexPath) as! BookRestaurantCell
        let obj = arrayRestaurant[indexPath.row]
        
        cell.labelTitle.text = isEnglish() ? obj.name:obj.name_ar
        cell.labelSubTitle.text = isEnglish() ? obj.cuisine.uppercased():obj.cuisine_ar
        
        if selectedRestaurant.id == obj.id {
            cell.viewBackground.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.6470588235, blue: 0.2431372549, alpha: 1)
        }else{
            cell.viewBackground.backgroundColor = #colorLiteral(red: 0.07058823529, green: 0.06666666667, blue: 0.06666666667, alpha: 1)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedRestaurant = arrayRestaurant[indexPath.row]
        collectionViewRestaurantList.reloadData()
    }
    
}
//MARK: Tableview Extension
extension BookATableVC:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewTimeHours {
            return self.arrayHours.count
        }
        if tableView == tableViewTimeMinutes {
            return self.arrayMinutes.count
        }
        return self.arrayPersons.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifierDropDown, for: indexPath) as! TimeCell
        var obj = ""
        
        if tableView == tableViewTimeHours {
            obj = arrayHours[indexPath.row]
            cell.viewIndicator.backgroundColor =  obj == labelTimeHour.text ? #colorLiteral(red: 0.8431372549, green: 0.6078431373, blue: 0.1960784314, alpha: 1):UIColor.white
        }else if tableView == tableViewTimeMinutes {
            obj = arrayMinutes[indexPath.row]
            cell.viewIndicator.backgroundColor =  obj == labelTimeMinutes.text ? #colorLiteral(red: 0.8431372549, green: 0.6078431373, blue: 0.1960784314, alpha: 1):UIColor.white
        }else{
            obj = arrayPersons[indexPath.row]
            cell.viewIndicator.backgroundColor =  obj == labelNumberOfPerson.text ? #colorLiteral(red: 0.8431372549, green: 0.6078431373, blue: 0.1960784314, alpha: 1):UIColor.white
        }
        
        cell.labelTitle.text = obj
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewTimeHours {
            labelTimeHour.text = arrayHours[indexPath.row]
        }else if tableView == tableViewTimeMinutes {
            labelTimeMinutes.text = arrayMinutes[indexPath.row]
        }else{
            labelNumberOfPerson.text = arrayPersons[indexPath.row]
        }
        tableView.reloadData()
        hideAllDropDown()
    }
    
    func hideAllDropDown(){
        viewTimeHours.isHidden = true
        viewTimeMinutes.isHidden = true
        viewNoOfPersons.isHidden = true
    }
}
