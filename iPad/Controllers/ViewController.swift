//
//  ViewController.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 11/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit
import Kingfisher
import MZDownloadManager
class ViewController: UIViewController {
    //    var arrayRestaurant = [Restaurant]()
    //    var arrayCategories = [Category]()
    //    var arrayDishes = [Dish]()
    let myDownloadPath = MZUtility.baseFilePath + "/My_Downloads"
    lazy var downloadManager: MZDownloadManager = {
        [unowned self] in
        let sessionIdentifer: String = "com.iosDevelopment.MZDownloadManager.BackgroundSession"
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var completion = appDelegate.backgroundSessionCompletionHandler
        
        let downloadmanager = MZDownloadManager(session: sessionIdentifer, delegate: self, completion: completion)
        return downloadmanager
        }()
    var arrayImages = [String]()
    
    
    var restaurantId = "37" //"21"
    var index = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        loadConstraint()
        if !FileManager.default.fileExists(atPath: myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        debugPrint("custom download path: \(myDownloadPath)")
        index = 0
        UIApplication.shared.statusBarStyle = .default
        if UserDefaults.standard.string(forKey: "update") == "1" {
            getTypes()
        }else if UserDefaults.standard.bool(forKey: "languageChanged"){
            goToNextScreen()
        }else{
            Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(goToNextScreen), userInfo: nil, repeats: false)
        }
        
//        getTypes()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @objc func goToNextScreen(){
        self.performSegue(withIdentifier: "LanguageSelectionVC", sender: self)
    }
}
extension ViewController{
    func getTags() {
        DispatchQueue.main.async {
            self.showLoading()
        }
        callService(Path.getTags, Method:.post,isWithLoading :false) { (json, error) in
           // print(json)
            self.deleteAllData("TagDB")
            for object in json["result"].arrayValue {
                let _ = Tag.init(fromJson: object)
            }
            self.getCategories()
        }
    }
    func getTypes() {
        callService(Path.getTypes, Method:.post,isWithLoading :false) { (json, error) in
            //print(json)
            self.deleteAllData("TypeDB")
            self.deleteAllData("CartDB")
            for object in json["result"].arrayValue {
                let _ = Type.init(fromJson: object)
            }
            self.getTags()
        }
    }
    
//    func getRestaurant() {
//        let param = ["":""]
//        callService(Path.getRestaurant, parameter: param,Method:.post,isWithLoading :false) { (json, error) in
//            print(json)
//            self.deleteAllData("RestaurantDB")
//            for result in json["result"].arrayValue {
//                for object in result["restaurants"].arrayValue {
//                    let obj = Restaurant.init(fromJson: object)
//                    let array = self.fetchData("TypeDB", id: result["type_id"].stringValue) as! [TypeDB]
//                    if array.count > 0 {
//                        obj.createDB(objectDB: array[0])
//                    }
////                    let imageView = UIImageView()
////                    imageView.SetImage(imageName: obj.background_image)
//                    if obj.background_image != "" {
//                        self.arrayImages.append(obj.background_image)
//                    }
//                    if obj.details_background_image != "" {
//                        self.arrayImages.append(obj.details_background_image)
//                    }
////                    let imageView1 = UIImageView()
////                    imageView1.SetImage(imageName: obj.logo)
//                    if obj.logo != "" {
//                        self.arrayImages.append(obj.logo)
//                    }
//                }
//            }
//            //print(self.arrayRestaurant.count)
//            self.getCategories()
//        }
//    }
    func getCategories() {
        let param = ["restaurant_id":restaurantId]
        callService(Path.getCategories, parameter: param,Method:.post,isWithLoading :false) { (json, error) in
            //print(json)
            self.deleteAllData("CategoryDB")
            for object in json["result"].arrayValue {
//                for object in result["categories"].arrayValue {
                    let obj = Category.init(fromJson: object)
                    if obj.logo != "" {
                        self.arrayImages.append(obj.logo)
                    }
//                }
            }
            self.getDishes()
        }
    }
    func getDishes() {
        let param = ["restaurant_id":restaurantId]
        
        callService(Path.getDishes, parameter: param,Method:.post,isWithLoading :false) { (json, error) in
           print(json)
            self.deleteAllData("DishDB")
            self.deleteAllData("GalleryDB")
            for result in json["result"].arrayValue {
                for object in result["dishes"].arrayValue {
                    let obj = Dish.init(fromJson: object,category_id: result["category_id"].stringValue)
                    let set = NSMutableSet()
                    for tag in obj.tags{
                        let tags = self.fetchData("TagDB", id: tag.filter_id) as! [TagDB]
                        set.addObjects(from: tags)
                    }
                    obj.createDB(tagset: set)
                    if obj.main_image != "" && !self.arrayImages.contains(obj.main_image) {
                        self.arrayImages.append(obj.main_image)
                    }
                    for url in obj.gallery {
                        if url != "" && !self.arrayImages.contains(url){
                            self.arrayImages.append(url)
                        }
                    }
                }
            }
            if json["result"].arrayValue.count > 0 {
                UserDefaults.standard.setValue("0", forKey: "update")
                UserDefaults.standard.synchronize()
                DispatchQueue.main.async {
//                    self.showLoading()
//                    self.downloadImage()
//                    for obj in self.arrayImages {
//                        let theFileName = (obj as NSString).lastPathComponent
//                        let myDownloadPath = MZUtility.baseFilePath + "/My_Downloads"
//                        let url = myDownloadPath + "/" + theFileName
//                        if !FileManager.default.fileExists(atPath: url) {
//                            self.downloadManager.addDownloadTask(theFileName, fileURL: obj, destinationPath: myDownloadPath)
//                        }
//                    }
                    UserDefaults.standard.setValue(self.arrayImages, forKey: "imageArray")
                    UserDefaults.standard.synchronize()
                    self.hideLoading()
                    self.performSegue(withIdentifier: "LanguageSelectionVC", sender: self)
                }
            }else{
                 DispatchQueue.main.async {
                    self.hideLoading()
                }
            }
        }
    }
}

extension ViewController : MZDownloadManagerDelegate {
    func downloadImage(){
        print("\(index)")
        if index < arrayImages.count {
            
            
           let theFileName = (arrayImages[index] as NSString).lastPathComponent
            let myDownloadPath = MZUtility.baseFilePath + "/My_Downloads"
            let url = myDownloadPath + "/" + theFileName
            if !FileManager.default.fileExists(atPath: url) {
                downloadManager.addDownloadTask(theFileName, fileURL: arrayImages[index], destinationPath: myDownloadPath)
            }else {
                self.index += 1
                downloadImage()
            }
        }else{
            print("Done")
            self.hideLoading()
            self.performSegue(withIdentifier: "LanguageSelectionVC", sender: self)
        }
    }
    func downloadRequestStarted(_ downloadModel: MZDownloadModel, index: Int) {
        
     
    }
    
    func downloadRequestDidPopulatedInterruptedTasks(_ downloadModels: [MZDownloadModel]) {
      
    }
    
    func downloadRequestDidUpdateProgress(_ downloadModel: MZDownloadModel, index: Int) {
     
    }
    
    func downloadRequestDidPaused(_ downloadModel: MZDownloadModel, index: Int) {
     
    }
    
    func downloadRequestDidResumed(_ downloadModel: MZDownloadModel, index: Int) {
      
    }
    
    func downloadRequestCanceled(_ downloadModel: MZDownloadModel, index: Int) {
        
      
    }
    
    func downloadRequestFinished(_ downloadModel: MZDownloadModel, index: Int) {
//         self.index += 1
//        downloadImage()
        
    }
    
    func downloadRequestDidFailedWithError(_ error: NSError, downloadModel: MZDownloadModel, index: Int) {
        debugPrint("Error while downloading file: \(downloadModel.fileName)  Error: \(error)")
    }
    
    //Oppotunity to handle destination does not exists error
    //This delegate will be called on the session queue so handle it appropriately
    func downloadRequestDestinationDoestNotExists(_ downloadModel: MZDownloadModel, index: Int, location: URL) {
        let myDownloadPath = MZUtility.baseFilePath + "/My_Downloads"
        if !FileManager.default.fileExists(atPath: myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        let fileName = MZUtility.getUniqueFileNameWithPath((myDownloadPath as NSString).appendingPathComponent(downloadModel.fileName as String) as NSString)
        let path =  myDownloadPath + "/" + (fileName as String)
        try! FileManager.default.moveItem(at: location, to: URL(fileURLWithPath: path))
        debugPrint("Default folder path: \(myDownloadPath)")
    }
}

