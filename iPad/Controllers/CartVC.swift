//
//  CartVC.swift
//  GH-iPad
//
//  Created by Pranav Goswami on 21/06/18.
//  Copyright © 2018 Pranav Goswami. All rights reserved.
//

import UIKit

class CartVC: UIViewController {

    @IBOutlet weak var tableViewData: UITableView!
    
    @IBOutlet weak var labelFood: UILabel!
    @IBOutlet weak var labelTax: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    
    @IBOutlet weak var viewOrder: UIView!
    
    let identifierDish = "DishCell"
    var arrayData = [CartDB]()
    
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var labelNoData: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewData.register(UINib(nibName: identifierDish,bundle: nil), forCellReuseIdentifier: identifierDish)
        arrayData = self.fetchAllData("CartDB") as! [CartDB]
        labelNoData.setLineSpacing(lineSpacing: 6)
        labelNoData.textAlignment = .center
        if arrayData.count == 0 {
            viewNoData.isHidden = false
        }
        countTotal()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.flipImageViews(subviews: self.view.subviews)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func buttonRemoveAllClicked(_ sender: UIButton) {
//        self.deleteAllData("CartDB")
//        self.goBack(UIButton())
        self.showConfirmAlert("", subTitle: "Would you like to clear the items from your cart?", yesTitle: "YES", noTitle: "NO", delegate: self)
    }
    
    @IBAction func buttonAddMoreClicked(_ sender: UIButton) {
        self.goBack(UIButton())
    }

    @IBAction func buttonApplyClicked(_ sender: UIButton) {
    
    }
    
    @IBAction func buttonCheckoutClicked(_ sender: UIButton) {
//        self.deleteAllData("CartDB")
//        self.viewOrder.isHidden = false
        self.showConfirmAlert("", subTitle: "Would you like to clear the items from your cart?", yesTitle: "YES", noTitle: "NO", delegate: self)
    }
    
    func countTotal(){
        var total = 0.000
        for obj in arrayData{
            let price:Double = (obj.dish?.price_number as! NSString).doubleValue
            total += (price * Double(obj.qty))
        }
        let per = total * 5 / 100
        labelTax.text = "\(String(format: "BD %.3f", per))"//"BD 4.999"
        labelFood.text = "\(String(format: "BD %.3f", total))"
        labelTotal.text = "\(String(format: "BD %.3f", total + per))"
        if arrayData.count == 0 {
            viewNoData.isHidden = false
        }
    }
    @IBAction func buttonAddToCart(_ sender: UIButton) {
        goBack(UIButton())
    }
}
extension CartVC : ConfirmAlertDelegate {
    func buttonYesClicked() {
        self.deleteAllData("CartDB")
        self.arrayData.removeAll()
        self.tableViewData.reloadData()
        countTotal()
    }
    func buttonNoClicked() {
        
    }
}

//MARK: Tableview Extension
extension CartVC:UITableViewDataSource,UITableViewDelegate {
    // MARK: - TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayData.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifierDish, for: indexPath) as! DishCell
        let obj = self.arrayData[indexPath.row]
        cell.labelName.text = isEnglish() ? obj.dish?.name! : obj.dish?.name_ar!
        let arrayCategory = self.fetchData("CategoryDB", id: (obj.dish?.categoryID)!) as! [CategoryDB]
        if arrayCategory.count > 0 {
            cell.labelDescription.text =  isEnglish() ? arrayCategory[0].name : arrayCategory[0].name_ar //isEnglish() ? obj.dish?.detail! : obj.dish?.detail_ar
        }else{
            cell.labelDescription.text = ""
        }
        
        // "Bolied soya beans with salt Edamame can be part of a calorie-controlled diet to \nlose weight bacause each 1/2 cup contains only 95 calaries" //
        cell.labelPrice.text = isEnglish() ? obj.dish?.price : obj.dish?.price_ar
        //        cell.labelDiscount.text = obj.price_number
        
//        var tags = ""
//        for tag in obj.dish?.tags!.allObjects as! [TagDB] {
//            if tags == "" {
//                tags = "  " + (isEnglish() ? tag.name! : tag.name_ar!)
//            }else{
//                tags = tags + ", " + (isEnglish() ? tag.name! : tag.name_ar!)
//            }
//        }
//        if tags == "" {
//            cell.viewTag.isHidden = true
//        }else{
//            cell.viewTag.isHidden = false
//        }
//        cell.labelTag.text = tags
        cell.labeQTY.text = "\(obj.qty)"
        cell.buttonAdd.tag = indexPath.row
        cell.buttonAdd.addTarget(self, action: #selector(removeDish(sender:)), for: UIControl.Event.touchUpInside)
        
        
        cell.buttonPlus.tag = indexPath.row
        cell.buttonPlus.addTarget(self, action: #selector(plusQTY(sender:)), for: UIControl.Event.touchUpInside)
        
        cell.buttonSubtract.tag = indexPath.row
        cell.buttonSubtract.addTarget(self, action: #selector(minusQTY(sender:)), for: UIControl.Event.touchUpInside)
        
        cell.imageViewLogo.setOfflineImage(imageName: (obj.dish?.main_image!)!)
        cell.selectionStyle = .none
        self.flipImageViews(subviews: cell.subviews)
        
    
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        selectedDish = arrayData[indexPath.row]
//        self.performSegue(withIdentifier: "DishDetailVC", sender: self)
    }
    @objc func plusQTY(sender:UIButton) {
        let obj = arrayData[sender.tag]
        obj.qty += 1
        tableViewData.reloadData()
        saveCart(sender: obj)
    }
    
    @objc func minusQTY(sender:UIButton) {
        let obj = arrayData[sender.tag]
        if obj.qty > 1 {
            obj.qty -= 1
            tableViewData.reloadData()
            saveCart(sender: obj)
        }
        
    }
    
    @objc func removeDish(sender:UIButton) {
        let obj = arrayData[sender.tag]
        getContext().delete(obj)
        arrayData = self.fetchAllData("CartDB") as! [CartDB]
        tableViewData.reloadData()
        countTotal()
        saveContext()
    }
    func saveCart(sender:CartDB) {
        
        let context = self.getContext()
        let obj = context.object(with: sender.objectID) as! CartDB
        obj.qty = sender.qty
        self.saveContext()
        countTotal()
    }
}
